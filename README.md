# Evaluating Software Repositories
### George Cullinane

This is a final year project regarding the evaluation of software repositories for both educational general usage.

You will be prompted to give the application access to your GitHub account immediately after following this link.

# FOR THE ASSESSOR/SUPERVISOR

In the interest of security, this repository does not contain the API keys necessary for running if you wish to run it locally. Files containing these API keys have been emailed to you on 01/05/2019.
Put both of these files in the folder comp3931/src/main/resources/properties/ before running to be able to run it locally.

## Running instructions

**After adding the properties files to the project**

To run the application locally, in the terminal navigate to the root folder of the project (comp3931) and run the command `./gradlew bootrun`
