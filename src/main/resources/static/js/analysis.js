console.log(messageLengths);
window.onload = function () {

    var dataPoints = [];
    var lowestMarked = false;
    var highestMarked = false;
    var counter = 0;
    messageLengths.forEach(function (element) {

        var currentMessage = messages[counter];
        var currentDate = dates[counter];

        if (element === lowest && lowestMarked === false) {
            dataPoints.push({
                y: element,
                x: new Date(currentDate),
                indexLabel: "lowest",
                markerColor: "DarkSlateGrey",
                markerType: "cross",
                toolTipContent: "(" + element + "): " + currentMessage
            });
            lowestMarked = true;
            console.log(lowestMarked);
        } else if (element === highest && highestMarked === false) {
            dataPoints.push({
                y: element,
                x: new Date(currentDate),
                indexLabel: "highest",
                markerColor: "red",
                markerType: "triangle",
                toolTipContent: "(" + element + "): " + currentMessage
            });
            highestMarked = true;
        } else {
            dataPoints.push({
                y: element,
                x: new Date(currentDate),
                toolTipContent: "(" + element + "): " + currentMessage
            });
        }
        counter++;
    });
    console.log(dataPoints);

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        zoomEnabled: true,
        theme: "light2",
        backgroundColor: "#fafbfc",
        title: {
            text: "Commit Message Lengths (Most Recent 100)"
        },
        axisY: {
            includeZero: false,
            title: "Message Lengths",

        },
        data: [{
            type: "line",
            dataPoints: dataPoints
        }]

    });
    console.log(chart);
    chart.render();

};

window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}