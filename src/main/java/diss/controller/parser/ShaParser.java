package diss.controller.parser;

import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShaParser {

    /**
     * @param commits a JSONObject containing a list of commits in a repository
     * @return the sha which appears in the last commit of the page
     */
    public String extractLastShaOnPage(JSONObject commits) {
        JSONArray commitArray = JSONUtils.retrieveJsonArrayOrEmpty(commits, "commits");
        JSONObject lastCommitOnPage = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(commitArray, 0);
        return (JSONUtils.retrieveStringOrEmpty(lastCommitOnPage, "sha"));
    }

    /**
     * @param commit a singular commit JSON representation
     * @return the sha of the commit detailed in the JSON
     */
    public String extractShaFromCommit(JSONObject commit) {
        return JSONUtils.retrieveStringOrEmpty(commit, "sha");
    }
}
