package diss.controller.parser;

import com.google.common.collect.Lists;
import diss.controller.domain.OrganisationRepository;
import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class OrganisationRepositoriesParser {

    /**
     * A method to parse the url of all of the repositories inside the organisation
     *
     * @param orgReposOuterJson a JSON object containing information about an organisation
     * @return A list of the urls of all of the repositories within the organistaion
     */
    public List<String> parseRepoList(JSONObject orgReposOuterJson) {

        List<String> repoList = Lists.newArrayList();
        JSONArray reposArray = JSONUtils.retrieveJsonArrayOrEmpty(orgReposOuterJson, "repositories");

        for (int i = 0; i < reposArray.length(); i++) {
            JSONObject currentRepo = reposArray.getJSONObject(i);
            repoList.add(JSONUtils.retrieveStringOrEmpty(currentRepo, "html_url"));
        }

        return repoList;
    }


    /**
     * A method to extract the name of the organisation
     *
     * @param orgReposOuterJson a JSON object containing information about an organisation
     * @return the name of the organisation
     */
    public String parseOrganisationName(JSONObject orgReposOuterJson) {
        JSONArray reposArray = JSONUtils.retrieveJsonArrayOrEmpty(orgReposOuterJson, "repositories");
        JSONObject firstRepo = reposArray.getJSONObject(0);
        JSONObject owner = JSONUtils.retrieveJsonObjectOrEmpty(firstRepo, "owner");
        return JSONUtils.retrieveStringOrEmpty(owner, "login");
    }

    /**
     * A method to parse the information on all of the repositories in an organisation.
     *
     * @param orgReposOuterJson a JSON object containing information about an organisation
     * @param accessToken       an access token, provided by GitHub
     * @param executeCodeLinter a flag indicating whether or not we wish to run the linter over the code base
     * @return a List of objects which contain links to analyse the repositories inside the organisation, as well some other information about the repositories such as their names
     */
    public List<OrganisationRepository> parseOrganisationRepositories(JSONObject orgReposOuterJson,
                                                                      String accessToken, boolean executeCodeLinter) {

        List<OrganisationRepository> organisationRepositoryList = Lists.newArrayList();
        JSONArray reposArray = JSONUtils.retrieveJsonArrayOrEmpty(orgReposOuterJson, "repositories");

        for (int i = 0; i < reposArray.length(); i++) {
            OrganisationRepository currentOrganisationRepository = new OrganisationRepository();
            JSONObject currentRepo = reposArray.getJSONObject(i);
            currentOrganisationRepository.setRepoLink((JSONUtils.retrieveStringOrEmpty(currentRepo, "html_url")));

            JSONObject ownerOuterJson = JSONUtils.retrieveJsonObjectOrEmpty(currentRepo, "owner");

            currentOrganisationRepository.setOwnerName((JSONUtils.retrieveStringOrEmpty(ownerOuterJson, "login")));
            currentOrganisationRepository.setFullName((JSONUtils.retrieveStringOrEmpty(currentRepo, "full_name")));
            String analysisLink = executeCodeLinter ?
                    "/getRepoAnalysis?repoURI=" + currentOrganisationRepository.getRepoLink()
                            + "&accessToken=" + accessToken
                            + "&executeCodeLinter=on" :
                    "/getRepoAnalysis?repoURI="
                            + currentOrganisationRepository.getRepoLink()
                            + "&accessToken=" + accessToken
                            + "&executeCodeLinter=off";


            currentOrganisationRepository.setAnalysisLink(analysisLink);
            organisationRepositoryList.add(currentOrganisationRepository);
        }

        return organisationRepositoryList;

    }
}
