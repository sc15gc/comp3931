package diss.controller;

import diss.api.GitHubService;
import diss.api.domain.Commit;
import diss.api.domain.User;
import diss.api.parser.CommitParser;
import diss.builder.AnalysisBuilder;
import diss.controller.domain.OrganisationRepository;
import diss.controller.parser.OrganisationRepositoriesParser;
import diss.controller.parser.ShaParser;
import diss.domain.Analysis;
import diss.parser.RepoNameParser;
import diss.parser.UserParser;
import diss.parser.utils.ParserUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Controller
public class AnalysisController {

    private final GitHubService gitHubService;
    private final AnalysisBuilder analysisBuilder;
    private final ShaParser shaParser;
    private final RepoNameParser repoNameParser;
    private final OrganisationRepositoriesParser organisationRepositoriesParser;
    private final UserParser userParser;
    private final CommitParser commitParser;
    private Logger LOG = LoggerFactory.getLogger(AnalysisController.class);

    @Autowired
    public AnalysisController(GitHubService gitHubService, AnalysisBuilder analysisBuilder, ShaParser shaParser,
                              RepoNameParser repoNameParser, OrganisationRepositoriesParser organisationRepositoriesParser,
                              UserParser userParser, CommitParser commitParser) {
        this.gitHubService = gitHubService;
        this.analysisBuilder = analysisBuilder;
        this.shaParser = shaParser;
        this.repoNameParser = repoNameParser;
        this.organisationRepositoriesParser = organisationRepositoriesParser;
        this.userParser = userParser;
        this.commitParser = commitParser;
    }

    /**
     * retrieves the analysis of repository on a given input. Input can either be a link to a repository
     * or an organisation.
     *
     * @param repoURI           the repository being evaluated
     * @param accessToken       The access token provided by GitHub
     * @param executeCodeLinter a flag indicating whether or not we wish to run the linter over the code base
     * @return a ModelAndView object which holds the analysis information
     * @throws IOException
     * @throws InterruptedException
     * @throws TimeoutException
     * @throws ExecutionException
     */
    @GetMapping("/analyse")
    public ModelAndView getAnalysis(@RequestParam(name = "repoURI") String repoURI,
                                    @RequestParam(name = "accessToken") String accessToken,
                                    @RequestParam(name = "executeCodeLinter", required = false) boolean executeCodeLinter) throws IOException, InterruptedException, TimeoutException, ExecutionException {


        LOG.info("Code linting: " + executeCodeLinter);

        String repoName = repoNameParser.parseRepoNameFromUri(repoURI);
        if (repoName.isEmpty()) {
            //TODO: implement handling when input is not organisation or repo i.e. user
            return buildOrganisationRepoListModel(repoURI, accessToken, executeCodeLinter);
        } else {
            return buildAnalysisForIndividualRepository(repoURI, accessToken, executeCodeLinter);
        }

    }

    private ModelAndView buildOrganisationRepoListModel(String repoURI, String accessToken, boolean executeCodeLinter) {

        try {

            CompletableFuture<JSONObject> organisationReposInformationFuture = gitHubService.retrieveAllRepositoriesInOrganisation(repoURI, accessToken);
            JSONObject organisationReposInformation = organisationReposInformationFuture.get(10000, TimeUnit.MILLISECONDS);

            List<OrganisationRepository> organisationRepositories = organisationRepositoriesParser.
                    parseOrganisationRepositories(organisationReposInformation, accessToken, executeCodeLinter);

            String orgName = getNameFromUri(repoURI);

            ModelAndView modelAndView = new ModelAndView("organisationPage");
            modelAndView.addObject("organisationRepositories", organisationRepositories);
            modelAndView.addObject("orgName", orgName);
            return modelAndView;
        } catch (Exception e) {
            LOG.error("Unable to retrieve list of repositories from organisation: " + e);
            return new ModelAndView();
        }

    }


    /**
     * Retrieves the analysis for an individual repository.
     *
     * @param repoURI           the repository being evaluated
     * @param accessToken       The access token provided by GitHub
     * @param executeCodeLinter a flag indicating whether or not we wish to run the linter over the code base
     * @return a ModelAndView object which holds the analysis information
     * @throws IOException
     * @throws InterruptedException
     * @throws TimeoutException
     * @throws ExecutionException
     */
    @GetMapping("/getRepoAnalysis")
    public ModelAndView getIndividualRepositoryAnalysis(@RequestParam(name = "repoURI") String repoURI,
                                                        @RequestParam(name = "accessToken") String accessToken,
                                                        @RequestParam(name = "executeCodeLinter", required = false) boolean executeCodeLinter) throws IOException, InterruptedException, TimeoutException, ExecutionException {

        return buildAnalysisForIndividualRepository(repoURI, accessToken, executeCodeLinter);
    }

    private ModelAndView buildAnalysisForIndividualRepository(String repoURI, String accessToken, boolean executeCodeLinter) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        CompletableFuture<JSONObject> repositoryDetails = gitHubService.retrieveRepoJsonAsync(repoURI, accessToken);
        CompletableFuture<JSONObject> ownerInfoFuture = gitHubService.retrieveRepoOwnerInfo(repoURI, accessToken);
        CompletableFuture<JSONObject> contents = gitHubService.retrieveContentsOfRepoAsync(repoURI, accessToken);
        CompletableFuture<JSONObject> recentCommits = gitHubService
                .retrievePageOfCommitsAsync(repoURI, 1, accessToken, 100);
        CompletableFuture<JSONObject> initialCommitFuture = gitHubService.retrieveInitialCommitAsync(repoURI, accessToken);
        CompletableFuture<Integer> amountOfContributors = gitHubService.retrieveAmountOfContributors(repoURI, accessToken);
        CompletableFuture<Integer> amountOfBranches = gitHubService.retrieveAmountOfBranches(repoURI, accessToken);
        CompletableFuture<Integer> amountOfTags = gitHubService.retrieveAmountOfTags(repoURI, accessToken);


        CompletableFuture.allOf(repositoryDetails, contents, recentCommits, ownerInfoFuture, amountOfBranches, amountOfContributors);

        JSONObject initialCommitJson = initialCommitFuture.get(10000, TimeUnit.MILLISECONDS);
        String firstSha = shaParser.extractShaFromCommit(initialCommitJson);
        CompletableFuture<JSONObject> detailedInitialCommitJsonFuture = gitHubService.retrieveSpecificCommitAsync(repoURI, firstSha, accessToken);
        JSONObject detailedInitialCommitJson = detailedInitialCommitJsonFuture.get(10000, TimeUnit.MILLISECONDS);

        Commit initialCommit = commitParser.parseCommit(detailedInitialCommitJson);

        User initialCommitter = userParser.parseUser(gitHubService.retrieveUser(initialCommit.getCommitterLogin(), accessToken).get(10000, TimeUnit.MILLISECONDS));

        String lastSha = shaParser.extractLastShaOnPage(recentCommits.get(10000, TimeUnit.MILLISECONDS));
        JSONObject comparison = gitHubService.compareTwoCommits(repoURI, accessToken, firstSha, lastSha);


        Analysis analysis = analysisBuilder.buildAnalysis(repoURI, accessToken, repositoryDetails.getNow(new JSONObject()),
                contents.getNow(new JSONObject()), recentCommits.getNow(null), comparison, executeCodeLinter);
        ModelAndView modelAndView = new ModelAndView("analysis");
        modelAndView.addObject("repoName", analysis.getRepoName());
        modelAndView.addObject("repoUri", repoURI);
        modelAndView.addObject("language", analysis.getLanguage());
        modelAndView.addObject("amountOfCommits", analysis.getAmountOfCommits());
        modelAndView.addObject("commits", analysis.getCommits());
        modelAndView.addObject("gitIgnorePresent", analysis.isGitIgnorePresent());
        modelAndView.addObject("circleCiPresent", analysis.isCircleCiPresent());
        modelAndView.addObject("readMePresent", analysis.isReadMePresent());
        modelAndView.addObject("readMe", analysis.getReadMeFile());
        modelAndView.addObject("ideaFolderPresent", analysis.isIdeaFolderPresent());
        modelAndView.addObject("dockerfilePresent", analysis.isDockerfilePresent());
        modelAndView.addObject("framework", analysis.getFramework());
        modelAndView.addObject("commitAnalytics", analysis.getCommitListAnalysis());
        modelAndView.addObject("isPrivate", analysis.isPrivate());
        modelAndView.addObject("lintOutput", analysis.getLintOutput());
        modelAndView.addObject("amountOfContributors", amountOfContributors.getNow(0));
        modelAndView.addObject("amountOfBranches", amountOfBranches.getNow(0));
        modelAndView.addObject("amountOfTags", amountOfTags.getNow(0));
        modelAndView.addObject("initialCommit", initialCommit);
        modelAndView.addObject("initialCommitter", initialCommitter);
        modelAndView.addObject("user", userParser.parseUser(ownerInfoFuture.getNow(null)));

        if (analysis.isReadMePresent()) {
            CompletableFuture<String> rawReadMeFuture = gitHubService.retrieveFromUrl(analysis.getReadMeFile().getRawUrl());
            String rawReadMe = rawReadMeFuture.get(10000, TimeUnit.MILLISECONDS);
            analysis.getReadMeFile().setContents(rawReadMe);
            CompletableFuture<String> markDown = gitHubService.convertToMarkdown(rawReadMe, accessToken);
            String readMeMarkdown = markDown.get(10000, TimeUnit.MILLISECONDS);
            modelAndView.addObject("readMeFile", readMeMarkdown);

        }
        return modelAndView;
    }

    private String getNameFromUri(String repoURI) {
        String path = ParserUtils.getPathFromGitHubUri(repoURI);
        String[] split_path = path.split("/");
        return split_path[1];
    }


}
