package diss.controller;

import diss.api.GitHubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Properties;

/**
 * Controller class to deal with the entry point to the website.
 */
@Controller
public class HomepageController {

    private final GitHubService gitHubService;

    private Logger LOG = LoggerFactory.getLogger(HomepageController.class);

    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    public HomepageController(GitHubService gitHubService) {
        this.gitHubService = gitHubService;

    }

    /**
     * A method which controls entry to the homepage of the application
     *
     * @param code the code to verify the application, provided by GitHub
     * @return A ModelAndView object containing the homepage of the application
     */
    @GetMapping("/homepage")
    public ModelAndView login(@RequestParam(name = "code", required = false) String code) {
        if (code == null) {
            return new ModelAndView("redirect:/login");
        }
        String accessToken = gitHubService.retrieveAccessToken(code, retrieveClientId(), retrieveClientSecret());
        ModelAndView modelAndView = new ModelAndView("homepage");
        modelAndView.addObject("accessToken", accessToken);
        return modelAndView;
    }

    /**
     * @param model
     * @return redirect to GitHub login page
     */
    @GetMapping("/login")
    public ModelAndView redirectUsingRedirectPrefix(ModelMap model) {
        return new ModelAndView("redirect:https://github.com/login/oauth/authorize?client_id=" + retrieveClientId() + "&scope=repo", model);
    }

    /**
     * A method to deal with the index url of the website
     *
     * @return a redirect to the homepage of the application
     */
    @GetMapping("/")
    public ModelAndView redirectIndex() {
        return new ModelAndView("redirect:/homepage");
    }

    private String retrieveClientSecret() {
        try {
            Properties apiProperties = new Properties();
            Resource resource = resourceLoader.getResource("classpath:properties/api.properties");
            apiProperties.load(resource.getInputStream());
            return (apiProperties.getProperty("github.client.secret"));
        } catch (Exception e) {
            LOG.error("Cannot locate GitHub Client Secret");
            return "Cannot find client secret!";
        }
    }

    private String retrieveClientId() {
        try {
            Properties apiProperties = new Properties();
            Resource resource = resourceLoader.getResource("classpath:properties/api.properties");
            apiProperties.load(resource.getInputStream());
            return (apiProperties.getProperty("github.client.id"));
        } catch (Exception e) {
            LOG.error("Cannot locate GitHub Client Id");
            return "Cannot find client ID!";
        }
    }

}
