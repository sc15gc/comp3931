package diss.controller.domain;

/**
 * A class to represent the information found about a repository within an organisation
 */
public class OrganisationRepository {

    private String fullName;
    private String repoLink;
    private String owner;
    private String analysisLink;

    public String getRepoLink() {
        return repoLink;
    }

    public void setRepoLink(String repoLink) {
        this.repoLink = repoLink;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwnerName(String owner) {
        this.owner = owner;
    }

    public String getAnalysisLink() {
        return analysisLink;
    }

    public void setAnalysisLink(String analysisLink) {
        this.analysisLink = analysisLink;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
