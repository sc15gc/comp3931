package diss;

import diss.api.GitHubService;
import diss.api.builder.CommitListBuilder;
import diss.api.domain.analysis.CommitBulkAnalyser;
import diss.api.parser.CommitParser;
import diss.api.parser.ModifiedFilesParser;
import diss.builder.AnalysisBuilder;
import diss.controller.AnalysisController;
import diss.controller.parser.OrganisationRepositoriesParser;
import diss.controller.parser.ShaParser;
import diss.engine.FrameworkDetectionEngine;
import diss.lint_utils.PmdAnalysisUtil;
import diss.parser.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Carries out bean configuration for the class
 */
@Configuration
@EnableAsync
public class ApplicationConfiguration {

    @Bean
    public GitHubService gitHubService() {
        return new GitHubService(accessTokenParser(), repoNameParser(), userNameParser());
    }

    @Bean
    public RepoNameParser repoNameParser() {
        return new RepoNameParser();
    }

    @Bean
    public AnalysisBuilder analysisBuilder() {
        return new AnalysisBuilder(commitListBuilder(), languageParser(), contentsParser(), commitBulkAnalyser(),
                aheadByParser(), frameworkDetectionEngine(), pmdAnalysisUtil());
    }

    @Bean
    public CommitListBuilder commitListBuilder() {
        return new CommitListBuilder(gitHubService(), commitParser());
    }

    @Bean
    public CommitBulkAnalyser commitBulkAnalyser() {
        return new CommitBulkAnalyser();
    }

    @Bean
    public AnalysisController analysisController() {
        return new AnalysisController(gitHubService(), analysisBuilder(), shaExtractor(), repoNameParser(), repoListParser(),
                userParser(), commitParser());
    }

    @Bean
    public AccessTokenParser accessTokenParser() {
        return new AccessTokenParser();
    }

    @Bean
    public RepositoryFilesParser contentsParser() {
        return new RepositoryFilesParser();
    }

    @Bean
    public ContributionsParser contributionsParser() {
        return new ContributionsParser();
    }

    @Bean
    public RepositoryDetailsParser languageParser() {
        return new RepositoryDetailsParser();
    }

    @Bean
    public NameParser userNameParser() {
        return new NameParser();
    }

    @Bean
    public CommitParser commitParser() {
        return new CommitParser(modifiedFilesParser(), userNameParser(), repoNameParser());
    }

    @Bean
    public ModifiedFilesParser modifiedFilesParser() {
        return new ModifiedFilesParser();
    }

    @Bean
    public AheadByParser aheadByParser() {
        return new AheadByParser();
    }

    @Bean
    public ShaParser shaExtractor() {
        return new ShaParser();
    }

    @Bean
    public FrameworkDetectionEngine frameworkDetectionEngine() {
        return new FrameworkDetectionEngine(gitHubService());
    }

    @Bean
    public OrganisationRepositoriesParser repoListParser() {
        return new OrganisationRepositoriesParser();
    }

    @Bean
    public PmdAnalysisUtil pmdAnalysisUtil() {
        return new PmdAnalysisUtil();
    }

    @Bean
    public UserParser userParser() {
        return new UserParser();
    }

    @Bean
    public Executor taskExecutor() {
        System.out.println("task executor active");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(128);
        executor.setMaxPoolSize(128);
        executor.setQueueCapacity(256);
        executor.setThreadNamePrefix("GithubLookup-");
        executor.initialize();
        return executor;
    }
}
