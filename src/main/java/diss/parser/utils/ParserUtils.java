package diss.parser.utils;

public class ParserUtils {

    /**
     * @param uri the input URI to the system
     * @return the path which succeeds the github.com, giving the user name and repository name.
     */
    public static String getPathFromGitHubUri(String uri) {
        String[] path = uri.split("(?i)github.com");
        return path[1];
    }


}
