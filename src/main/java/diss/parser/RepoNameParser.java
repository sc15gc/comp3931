package diss.parser;

import diss.parser.utils.ParserUtils;

public class RepoNameParser {

    /**
     * A method which extracts the name of the repository from the GitHub link given
     *
     * @param uri the uri which contains the name of the repository
     * @return the name of the repository
     */
    public String parseRepoNameFromUri(String uri) {
        try {
            String path = ParserUtils.getPathFromGitHubUri(uri);
            String[] split_path = path.split("/");
            return split_path[2];
        } catch (ArrayIndexOutOfBoundsException e) {
            return "";
        }
    }

}
