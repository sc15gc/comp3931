package diss.parser;

import diss.api.domain.User;
import diss.utils.JSONUtils;
import org.json.JSONObject;

/**
 * A class to parse a user object
 */
public class UserParser {

    /**
     * A method to parse information about the User from the JSON object requested from GitHub
     *
     * @param userJson JSON provided by GitHub containing information about the user
     * @return A User object which has extracted out the necessary details from the JSON provided
     */
    public User parseUser(JSONObject userJson) {
        User user = new User();
        user.setName(JSONUtils.retrieveStringOrEmpty(userJson, "login"));
        user.setEmail(JSONUtils.retrieveStringOrEmpty(userJson, "email"));
        user.setAvatarUrl(JSONUtils.retrieveStringOrEmpty(userJson, "avatar_url"));
        return user;
    }

}
