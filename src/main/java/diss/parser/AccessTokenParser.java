package diss.parser;

import java.util.StringTokenizer;

/**
 * A class which parses the access token from the GitHub text which includes one
 */
public class AccessTokenParser {


    /**
     * @param returnParams The response which GitHub provides when requesting an access token
     * @return The access token
     */
    public String parseAccessCode(String returnParams) {
        StringTokenizer stringTokenizer = new StringTokenizer(returnParams, "=&");
        if (stringTokenizer.hasMoreTokens()) {
            stringTokenizer.nextToken();
            return stringTokenizer.nextToken();
        }
        return "";
    }

}
