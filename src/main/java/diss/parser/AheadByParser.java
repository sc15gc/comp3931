package diss.parser;

import diss.utils.JSONUtils;
import org.json.JSONObject;

public class AheadByParser {

    /**
     * @param comparison The JSON object which is returned when requesting a comparison from the GitHub API
     * @return An int representing the extraction of the 'ahead-by' field from the JSON object
     */
    public int retrieveAheadBy(JSONObject comparison) {
        return JSONUtils.retrieveIntOrEmpty(comparison, "ahead_by");
    }
}
