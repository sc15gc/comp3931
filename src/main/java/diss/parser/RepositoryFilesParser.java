package diss.parser;

import com.google.common.collect.Lists;
import diss.api.domain.File;
import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * A class containing methods to extract information from files held within a repository
 */
public class RepositoryFilesParser {

    /**
     * A method which detects whether a repository contains a .gitignore file
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a boolean which is true if a .gitignore file is present in the repository
     */
    public boolean hasGitIgnore(JSONObject contentsJson) {
        JSONArray contents = contentsJson.getJSONArray("contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = contents.getJSONObject(i);
            if (JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name").toLowerCase().equals(".gitignore")) {
                return true;
            }
        }
        return false;
    }

    /**
     * A method to extract the read me file from the repository
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a File object containing the read me file for the repository
     */
    public File extractReadMeFile(JSONObject contentsJson) {

        File readme = new File();
        JSONArray contents = contentsJson.getJSONArray("contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = contents.getJSONObject(i);
            if (JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name").toLowerCase().split("\\.")[0]
                    .equals("readme")) {
                readme.setRawUrl(JSONUtils.retrieveStringOrEmpty(currentJsonObject, "download_url"));
                readme.setFilename(JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name"));
            }
        }
        return readme;
    }

    /**
     * A method which detects whether a repository contains a .idea folder
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a boolean which is true if a .idea folder is present in the repository
     */
    public boolean hasIdeaFolder(JSONObject contentsJson) {
        JSONArray contents = JSONUtils.retrieveJsonArrayOrEmpty(contentsJson, "contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = contents.getJSONObject(i);
            if (JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name").toLowerCase().equals(".idea")) {
                return true;
            }
        }
        return false;
    }

    /**
     * A method which detects whether a repository contains a dockerfile
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a boolean which is true if a dockerfile is present in the repository
     */
    public boolean hasDockerfile(JSONObject contentsJson) {
        JSONArray contents = JSONUtils.retrieveJsonArrayOrEmpty(contentsJson, "contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(contents, i);
            if (JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name").toLowerCase().equals("dockerfile")) {
                return true;
            }
        }
        return false;
    }

    /**
     * A method which detects whether a repository contains a circleci folder
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a boolean which is true if a .circleci folder is present in the repository
     */
    public boolean hasCircleCi(JSONObject contentsJson) {
        JSONArray contents = JSONUtils.retrieveJsonArrayOrEmpty(contentsJson, "contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(contents, i);
            if (JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name").toLowerCase().equals(".circleci")) {
                return true;
            }
        }
        return false;
    }

    /**
     * A method which extracts all files from the top level of the repository
     *
     * @param contentsJson a JSONObject which contains the contents of the repository
     * @return a boolean which is true if a .idea folder is present in the repository
     */
    public List<File> retrieveAllFiles(JSONObject contentsJson) {
        List<File> filesList = Lists.newArrayList();
        JSONArray contents = JSONUtils.retrieveJsonArrayOrEmpty(contentsJson, "contents");
        for (int i = 0; i < contents.length(); i++) {
            JSONObject currentJsonObject = contents.getJSONObject(i);
            File file = new File();
            file.setFilename(JSONUtils.retrieveStringOrEmpty(currentJsonObject, "name"));
            file.setRawUrl(JSONUtils.retrieveStringOrEmpty(currentJsonObject, "download_url"));
            filesList.add(file);
        }
        return filesList;
    }
}
