package diss.parser;

import diss.utils.JSONUtils;
import org.json.JSONObject;

/**
 * A class which parses the details of the repository from a JSON object which contains an overview of the repository
 */
public class RepositoryDetailsParser {

    /**
     * parses the language used in the repository
     *
     * @param repo A JSON object which gives an overview of the repository
     * @return The language which the repository largely uses
     */
    public String parseLanguage(JSONObject repo) {
        return JSONUtils.retrieveStringOrEmpty(repo, "language");
    }

    /**
     * @param repo A JSON object which gives an overview of the repository
     * @return the name of the repository which the JSON object is about
     */
    public String parseRepoName(JSONObject repo) {
        return JSONUtils.retrieveStringOrEmpty(repo, "name");
    }

    /**
     * @param repo A JSON object which gives an overview of the repository
     * @return the privacy setting of the repository
     */
    public boolean parsePrivacySetting(JSONObject repo) {
        return JSONUtils.retrieveBooleanOrFalse(repo, "private");
    }
}
