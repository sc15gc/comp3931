package diss.parser;

import diss.parser.utils.ParserUtils;

/**
 * A class which parses either the username or repository name from a link to GitHub
 */
public class NameParser {

    public String parse(String uri) {
        String path = ParserUtils.getPathFromGitHubUri(uri);
        String[] split_path = path.split("/");
        return split_path[1];
    }

}
