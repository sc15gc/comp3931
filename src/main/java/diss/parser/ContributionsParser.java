package diss.parser;

import org.json.JSONObject;

public class ContributionsParser {

    /**
     * @param contributor JSON object which contains information regarding a contributor to a repository
     * @return the amount of contributions which this contributor made.
     */
    public int parseContributions(JSONObject contributor) {
        return contributor.getInt("contributions");
    }
}
