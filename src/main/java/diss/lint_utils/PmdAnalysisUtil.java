package diss.lint_utils;

import com.google.common.collect.Lists;
import diss.lint_utils.domain.Suggestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class to generate the lint output over a repository
 */
public class PmdAnalysisUtil {

    private static final Logger logger = LoggerFactory.getLogger(PmdAnalysisUtil.class);


    /**
     * @param repoURI     A repository to be analysed
     * @param accessToken An access token (provided by GitHub)
     * @return An ArrayList of suggestions regarding improvements that could be made to the code of the repository
     */
    public ArrayList<Suggestion> generateSuggestionsForCodeInRepository(String repoURI, String accessToken) {

        ArrayList<Suggestion> codeSuggestions = Lists.newArrayList();

        logger.info(String.format("Retrieving git repo: %s", repoURI));


        String oauthUrl = buildOAuthUrl(repoURI, accessToken);
        String repoDirectory = "repo_store/" + generateRandomString(4);
        new File(repoDirectory).mkdirs();
        ProcessBuilder pmdProcessBuilder = new ProcessBuilder();
        try {
            ArrayList<String> cloneCommand = Lists.newArrayList();
            cloneCommand.add("git");
            cloneCommand.add("clone");
            cloneCommand.add(oauthUrl);
            new File(repoDirectory);
            ProcessBuilder cloneProcessBuilder = new ProcessBuilder();
            cloneProcessBuilder.directory(new File(repoDirectory));
            cloneProcessBuilder.command(cloneCommand);
            cloneProcessBuilder.start().waitFor(10000, TimeUnit.MILLISECONDS);

            logger.info("Cloned repository: " + repoURI);
        } catch (Exception e) {
            logger.error("Could not clone repository: " + repoURI);
        }

        ArrayList<String> commands = Lists.newArrayList();

        commands.add("pmd-bin-6.12.0/bin/run.sh");
        commands.add("pmd");
        commands.add("-d");
        commands.add(repoDirectory);
        commands.add("-R");
        commands.add("rulesets/java/quickstart.xml");
        commands.add("-f");
        commands.add("text");
        pmdProcessBuilder.command(commands);

        Process pmdProcess;
        try {
            pmdProcess = pmdProcessBuilder.start();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(pmdProcess.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                String originalFilePath = parseFilePathFromSuggestion(line);
                String suggestion = parseSuggestionRaw(line);
                String lineNumber = parseLineNumberOfSuggestion(line);
                codeSuggestions.add(new Suggestion(originalFilePath, suggestion, repoURI, lineNumber));
            }

            logger.info("Cloned repository " + repoURI);
        } catch (Exception e) {
            logger.error("Failed to run linter over repository. " + e.toString());

        }

        cleanUpSpecifiedRepoStore(repoDirectory);
        return codeSuggestions;
    }

    String buildOAuthUrl(String repoURI, String accessToken) {
        repoURI = repoURI.replace("https://", "");
        repoURI = repoURI.replace("www.", "");
        return "https://" + accessToken + "@" + repoURI;
    }


    void cleanUpSpecifiedRepoStore(String repoDir) {
        File repo_store = new File(repoDir);
        delete(repo_store);
    }

    void delete(File file) {
        if (file.isDirectory()) {
            for (File currentFile : file.listFiles()) {
                delete(currentFile);
            }
        }

        file.delete();
    }

    String generateRandomString(int size) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Random r = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < size; i++) {
            int currentLocation = r.nextInt(26);
            stringBuilder.append(alphabet[currentLocation]);
        }
        return stringBuilder.toString();
    }

    String parseFilePathFromSuggestion(String suggestion) {
        String repoStoreLocation = retrieveRepoStoreLocation();

        return suggestion.split(repoStoreLocation)[1].replaceFirst("[A-Z]+/[A-Za-z0-9\\-_]+/", "")
                .replaceFirst(":[\\s\\S]*", "");
    }

    static String retrieveRepoStoreLocation() {
        return "/comp3931/repo_store/";
    }

    String parseFileAndSuggestionText(String suggestion) {
        Pattern pattern = Pattern.compile("[A-Za-z0-9]*\\.[A-Za-z]*:[0-9]*:[ \\t].*");
        Matcher matcher = pattern.matcher(suggestion);
        matcher.find();
        return matcher.group();
    }

    String parseLineNumberOfSuggestion(String suggestion) {
        String suggestionText = parseFileAndSuggestionText(suggestion);
        Pattern pattern = Pattern.compile(":[0-9]*:");
        Matcher matcher = pattern.matcher(suggestionText);
        matcher.find();
        return matcher.group().replaceAll(":", "");
    }

    String parseSuggestionRaw(String suggestion) {
        return parseFileAndSuggestionText(suggestion).split(":[0-9]*:")[1];
    }

}


