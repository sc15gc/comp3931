package diss.lint_utils.domain;

/**
 * A class to represent a code improvement suggestion from the Pmd linter
 */
public class Suggestion {

    private String originalFilePath;
    private String suggestionTextIncludingLocationOfSuggestion;
    private String repoURI;
    private String lineNumber;


    public Suggestion(String originalFilePath, String suggestionTextIncludingLocationOfSuggestion, String repoURI, String lineNumber) {
        this.originalFilePath = originalFilePath;
        this.suggestionTextIncludingLocationOfSuggestion = suggestionTextIncludingLocationOfSuggestion;
        this.repoURI = repoURI;
        this.lineNumber = lineNumber;
    }


    public String getLineNumber() {
        return lineNumber;
    }

    public String getRepoURI() {
        return repoURI;
    }

    public void setRepoURI(String repoURI) {
        this.repoURI = repoURI;
    }


    public String getOriginalFilePath() {
        return originalFilePath;
    }

    public String getSuggestionText() {
        return suggestionTextIncludingLocationOfSuggestion;
    }

    public String getGitHubLinkToFileOnTreeMaster() {
        return repoURI + "/blob/master/" + originalFilePath + "#L" + lineNumber;
    }

}
