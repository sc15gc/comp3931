package diss.builder;

import com.google.common.collect.Lists;
import diss.api.builder.CommitListBuilder;
import diss.api.domain.Commit;
import diss.api.domain.analysis.CommitBulkAnalyser;
import diss.api.domain.analysis.CommitListAnalysis;
import diss.domain.Analysis;
import diss.engine.FrameworkDetectionEngine;
import diss.lint_utils.PmdAnalysisUtil;
import diss.lint_utils.domain.Suggestion;
import diss.parser.AheadByParser;
import diss.parser.RepositoryDetailsParser;
import diss.parser.RepositoryFilesParser;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * A class which builds the analysis of the repository, using multiple parsers to extract information from the JSON
 * objects requested from the GitHub API
 */
@Component
public class AnalysisBuilder {

    private CommitListBuilder commitListBuilder;
    private RepositoryDetailsParser repositoryDetailsParser;
    private RepositoryFilesParser repositoryFilesParser;
    private CommitBulkAnalyser commitBulkAnalyser;
    private AheadByParser aheadByParser;
    private FrameworkDetectionEngine frameworkDetectionEngine;
    private PmdAnalysisUtil pmdAnalysisUtil;

    @Autowired
    public AnalysisBuilder(CommitListBuilder commitListBuilder,
                           RepositoryDetailsParser repositoryDetailsParser, RepositoryFilesParser repositoryFilesParser,
                           CommitBulkAnalyser commitBulkAnalyser,
                           AheadByParser aheadByParser,
                           FrameworkDetectionEngine frameworkDetectionEngine,
                           PmdAnalysisUtil pmdAnalysisUtil) {

        this.repositoryDetailsParser = repositoryDetailsParser;
        this.repositoryFilesParser = repositoryFilesParser;
        this.commitListBuilder = commitListBuilder;
        this.commitBulkAnalyser = commitBulkAnalyser;
        this.aheadByParser = aheadByParser;
        this.frameworkDetectionEngine = frameworkDetectionEngine;
        this.pmdAnalysisUtil = pmdAnalysisUtil;
    }

    public Analysis buildAnalysis(String repoURI, String accessToken,
                                  JSONObject repositoryDetails,
                                  JSONObject contents,
                                  JSONObject commits,
                                  JSONObject comparison,
                                  boolean executeCodeLinter) {
        Analysis analysis = new Analysis();
        analysis.setRepoName(repositoryDetailsParser.parseRepoName(repositoryDetails));
        analysis.setLanguage(repositoryDetailsParser.parseLanguage(repositoryDetails));
        analysis.setPrivate(repositoryDetailsParser.parsePrivacySetting(repositoryDetails));
        List<Commit> commitList = commitListBuilder.buildCommitList(repoURI, accessToken, commits);
        analysis.setCommits(commitList);
        CommitListAnalysis commitListAnalysis = commitBulkAnalyser.analyse(commitList);
        analysis.setCommitListAnalysis(commitListAnalysis);

        analysis.setGitIgnorePresent(repositoryFilesParser.hasGitIgnore(contents));
        analysis.setCircleCiPresent(repositoryFilesParser.hasCircleCi(contents));
        analysis.setReadMeFile(repositoryFilesParser.extractReadMeFile(contents));
        analysis.setIdeaFolderPresent(repositoryFilesParser.hasIdeaFolder(contents));
        analysis.setDockerfilePresent(repositoryFilesParser.hasDockerfile(contents));
        analysis.setAmountOfCommits(aheadByParser.retrieveAheadBy(comparison) + 1);
        analysis.setFramework(frameworkDetectionEngine.detectFramework(repositoryFilesParser.retrieveAllFiles(contents)));

        List<Suggestion> lintOutput =
                executeCodeLinter ? pmdAnalysisUtil.generateSuggestionsForCodeInRepository(repoURI, accessToken)
                        : Lists.newArrayList();

        analysis.setLintOutput(lintOutput);

        return analysis;
    }
}
