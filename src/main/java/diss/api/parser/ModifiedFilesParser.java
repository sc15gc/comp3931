package diss.api.parser;

import com.google.common.collect.Lists;
import diss.api.domain.ModifiedFile;
import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class ModifiedFilesParser {

    /**
     * A method to parse the files modified in a commit from a JSON object providing details of the commits
     *
     * @param outerCommitJson a JSON object representing an overview of a commit
     * @return a list of modified files in the commit
     */
    List<ModifiedFile> parse(JSONObject outerCommitJson) {
        List<ModifiedFile> modifiedFiles = Lists.newArrayList();
        JSONArray files = JSONUtils.retrieveJsonArrayOrEmpty(outerCommitJson, "files");
        for (int i = 0; i < files.length(); i++) {
            JSONObject currentFile = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(files, i);
            ModifiedFile modifiedFile = new ModifiedFile();
            modifiedFile.setFilename(JSONUtils.retrieveStringOrEmpty(currentFile, "filename"));
            modifiedFile.setAmountOfChanges(JSONUtils.retrieveIntOrEmpty(currentFile, "changes"));
            modifiedFile.setBlobUrl(JSONUtils.retrieveStringOrEmpty(currentFile, "blob_url"));
            modifiedFiles.add(modifiedFile);
        }
        return modifiedFiles;
    }
}
