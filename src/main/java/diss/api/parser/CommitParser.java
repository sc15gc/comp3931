package diss.api.parser;

import com.google.common.collect.Lists;
import diss.api.GitHubService;
import diss.api.domain.Commit;
import diss.parser.RepoNameParser;
import diss.parser.NameParser;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static diss.utils.JSONUtils.retrieveJsonObjectOrEmpty;
import static diss.utils.JSONUtils.retrieveStringOrEmpty;

public class CommitParser {

    private ModifiedFilesParser modifiedFilesParser;
    private RepoNameParser repoNameParser;
    private NameParser nameParser;

    private Logger logger = LoggerFactory.getLogger(CommitParser.class);

    @Autowired
    public CommitParser(ModifiedFilesParser modifiedFilesParser,
                        NameParser nameParser,
                        RepoNameParser repoNameParser) {
        this.modifiedFilesParser = modifiedFilesParser;
        this.nameParser = nameParser;
        this.repoNameParser = repoNameParser;
    }

    /**
     * A method to parse the information about a commit from a JSON object into a Commit object
     *
     * @param outerCommitJson a JSON object containing the detailed information about a commit
     * @return A Commit object containing the detailed information gathered from the JSON object in it.
     */
    public Commit parseCommit(JSONObject outerCommitJson) {
        Commit commit = new Commit();
        commit.setSha(retrieveStringOrEmpty(outerCommitJson, "sha"));
        JSONObject innerCommit = retrieveJsonObjectOrEmpty(outerCommitJson, "commit");
        JSONObject author = retrieveJsonObjectOrEmpty(outerCommitJson, "author");
        String message = retrieveStringOrEmpty(innerCommit, "message");
        JSONObject committer = retrieveJsonObjectOrEmpty(innerCommit, "committer");
        JSONObject innerAuthor = retrieveJsonObjectOrEmpty(innerCommit, "author");
        String dateString = retrieveStringOrEmpty(committer, "date");
        String nameOfCommitter = retrieveStringOrEmpty(innerAuthor, "name");
        String emailOfCommitter = retrieveStringOrEmpty(innerAuthor, "email");
        String login = retrieveStringOrEmpty(author, "login");
        commit.setMessage(message);
        commit.setCommitterLogin(login);
        commit.setCommitterName(nameOfCommitter);
        commit.setCommitterEmailAddress(emailOfCommitter);
        commit.setCommitDiffUrl(buildCommitDiffUrl(outerCommitJson));

        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            Date date = formatter.parse(dateString);
            commit.setDate(date);
        } catch (ParseException e) {
            logger.error("Time parsing exception from the commit: " + e.toString());
            commit.setDate(new Date());
        }
        commit.setModifiedFiles(modifiedFilesParser.parse(outerCommitJson));
        return commit;
    }

    /**
     * A method to parse the information from a list of JSON objects which detail individual commits
     *
     * @param commitListJson A list of JSON objects which contain JSON objects detailing individual commits
     * @return A list of Commit Objects
     */
    public List<Commit> parseListOfCommits(List<CompletableFuture<JSONObject>> commitListJson) {
        List<Commit> commits = Lists.newArrayList();
        try {
            for (CompletableFuture<JSONObject> outerCommitJsonFuture : commitListJson) {
                JSONObject outerCommitJson = outerCommitJsonFuture.get(10000, TimeUnit.MILLISECONDS);
                commits.add(parseCommit(outerCommitJson));
            }
        } catch (Exception e) {
            logger.error("Failed to parse list of commits");
        }
        return commits;
    }

    private String buildCommitDiffUrl(JSONObject jsonObject) {
        String htmlUrl = retrieveStringOrEmpty(jsonObject, "html_url");
        String sha = retrieveStringOrEmpty(jsonObject, "sha");
        String user = nameParser.parse(htmlUrl);
        String reponame = repoNameParser.parseRepoNameFromUri(htmlUrl);

        return GitHubService.getGitHubBase() + "/" + user + "/" + reponame + "/" + "commit" + "/" + sha;
    }

}
