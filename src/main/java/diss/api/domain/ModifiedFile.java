package diss.api.domain;

/**
 * A class to represent a file which has been modified in a commit
 */
public class ModifiedFile extends File {
    private String filename;
    private int amountOfChanges;
    private String blobUrl;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getAmountOfChanges() {
        return amountOfChanges;
    }

    public void setAmountOfChanges(int amountOfChanges) {
        this.amountOfChanges = amountOfChanges;
    }

    public String getBlobUrl() {
        return blobUrl;
    }

    public void setBlobUrl(String blobUrl) {
        this.blobUrl = blobUrl;
    }
}
