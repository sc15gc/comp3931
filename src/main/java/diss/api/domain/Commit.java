package diss.api.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Represents a commit in a Git repository
 */
public class Commit {

    private String message;
    private int number;
    private String committerName;
    private List<ModifiedFile> modifiedFiles;
    private String nodeId;
    private Date date;
    private String sha;
    private String commitDiffUrl;
    private String committerEmailAddress;
    private String committerLogin;


    public String getCommitterLogin() {
        return committerLogin;
    }

    public void setCommitterLogin(String committerLogin) {
        this.committerLogin = committerLogin;
    }


    public String getCommitDiffUrl() {
        return commitDiffUrl;
    }

    public void setCommitDiffUrl(String commitDiffUrl) {
        this.commitDiffUrl = commitDiffUrl;
    }


    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCommitterName() {
        return committerName;
    }

    public void setCommitterName(String committerName) {
        this.committerName = committerName;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public List<ModifiedFile> getModifiedFiles() {
        return modifiedFiles;
    }

    public void setModifiedFiles(List<ModifiedFile> modifiedFiles) {
        this.modifiedFiles = modifiedFiles;
    }

    @DateTimeFormat(pattern = "dd-MMM-YYYY HH:mm")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCommitterEmailAddress() {
        return committerEmailAddress;
    }

    public void setCommitterEmailAddress(String committerEmailAddress) {
        this.committerEmailAddress = committerEmailAddress;
    }
}
