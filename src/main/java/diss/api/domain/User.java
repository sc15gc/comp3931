package diss.api.domain;

/**
 * A class to represent a GitHub user
 */
public class User {

    private String login;
    private String avatarUrl;
    private String email;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return login;
    }

    public void setName(String name) {
        this.login = name;
    }
}
