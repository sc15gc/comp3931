package diss.api.domain.analysis;

import com.google.common.collect.Lists;
import diss.api.domain.Commit;

import java.util.IntSummaryStatistics;
import java.util.List;

public class CommitBulkAnalyser {

    /**
     * A method to analyse a list of commits
     *
     * @param commits The list of commits to be analysed.
     * @return A CommitListAnalysis bulk-overview analysis of all of the commits
     */
    public CommitListAnalysis analyse(List<Commit> commits) {
        CommitListAnalysis analysis = new CommitListAnalysis();
        int currentMaxLengthOfMessage = Integer.MIN_VALUE;
        int currentMinLengthOfMessage = Integer.MAX_VALUE;
        List<Integer> commitMessageLengths = Lists.newArrayList();
        List<String> rawCommitMessages = Lists.newArrayList();
        List<Long> dates = Lists.newArrayList();
        Commit commitWithMaxMessageLength = new Commit();
        Commit commitWithMinMessageLength = new Commit();

        for (Commit commit : commits) {
            int currentLengthOfCommitMessage = commit.getMessage().length();
            commitMessageLengths.add(currentLengthOfCommitMessage);
            rawCommitMessages.add(commit.getMessage());
            dates.add(commit.getDate().getTime());
            if (currentLengthOfCommitMessage > currentMaxLengthOfMessage) {
                currentMaxLengthOfMessage = currentLengthOfCommitMessage;
                commitWithMaxMessageLength = commit;
            }
            if (currentLengthOfCommitMessage < currentMinLengthOfMessage) {
                currentMinLengthOfMessage = currentLengthOfCommitMessage;
                commitWithMinMessageLength = commit;
            }
        }

        IntSummaryStatistics stats = commitMessageLengths.stream().mapToInt(Integer::intValue).summaryStatistics();
        analysis.setAverageLengthOfCommitMessage((Math.round(stats.getAverage() * 10d)) / 10d);
        analysis.setStandardDeviationOfCommitMessageLengths(calculateStandardDeviation(commitMessageLengths, stats.getAverage()));


        analysis.setMaxCommitLengthMessage(commitWithMaxMessageLength);
        analysis.setMinCommitLengthMessage(commitWithMinMessageLength);
        analysis.setCommitMessageLengths(commitMessageLengths);
        analysis.setRawMessages(rawCommitMessages);
        analysis.setDates(dates);
        return analysis;
    }

    double calculateStandardDeviation(List<Integer> commitLengths, double mean) {
        System.out.println(System.currentTimeMillis());
        List<Double> squaredResults = Lists.newArrayList();

        commitLengths.forEach(element ->
                squaredResults.add(Math.pow((mean - element), 2))
        );

        double sum = squaredResults.stream().mapToDouble(d -> d).sum();
        return Math.sqrt(sum / squaredResults.size());

    }
}
