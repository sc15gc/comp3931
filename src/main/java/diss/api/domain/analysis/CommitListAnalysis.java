package diss.api.domain.analysis;

import diss.api.domain.Commit;

import java.util.List;

/**
 * A class which represents the analysis over a multitude of commits
 */
public class CommitListAnalysis {

    private double averageLengthOfCommitMessage;
    private Commit maxCommitLengthMessage;
    private Commit minCommitLengthMessage;
    private double standardDeviationOfCommitMessageLengths;
    private List<Integer> commitMessageLengths;
    private List<String> rawMessages;
    private List<Long> dates;


    public Commit getMaxCommitMessageLengthObject() {
        return maxCommitLengthMessage;
    }

    public void setMaxCommitLengthMessage(Commit maxCommitLengthMessage) {
        this.maxCommitLengthMessage = maxCommitLengthMessage;
    }

    public Commit getMinCommitMessageLengthObject() {
        return minCommitLengthMessage;
    }

    public void setMinCommitLengthMessage(Commit minCommitLengthMessage) {
        this.minCommitLengthMessage = minCommitLengthMessage;
    }

    public double getAverageLengthOfCommitMessage() {
        return averageLengthOfCommitMessage;
    }

    public void setAverageLengthOfCommitMessage(double averageLengthOfCommitMessage) {
        this.averageLengthOfCommitMessage = averageLengthOfCommitMessage;
    }

    public double getStandardDeviationOfCommitMessageLengths() {
        return standardDeviationOfCommitMessageLengths;
    }

    public void setStandardDeviationOfCommitMessageLengths(double standardDeviationOfCommitMessageLengths) {
        this.standardDeviationOfCommitMessageLengths = standardDeviationOfCommitMessageLengths;
    }

    public List<Integer> getCommitMessageLengths() {
        return commitMessageLengths;
    }

    public void setCommitMessageLengths(List<Integer> commitMessageLengths) {
        this.commitMessageLengths = commitMessageLengths;
    }

    public List<String> getRawMessages() {
        return rawMessages;
    }

    public void setRawMessages(List<String> rawMessages) {
        this.rawMessages = rawMessages;
    }

    public List<Long> getDates() {
        return dates;
    }

    public void setDates(List<Long> dates) {
        this.dates = dates;
    }
}
