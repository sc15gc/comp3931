package diss.api.builder;

import com.google.common.collect.Lists;
import diss.api.GitHubService;
import diss.api.domain.Commit;
import diss.api.parser.CommitParser;
import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class CommitListBuilder {

    private static final Logger logger = LoggerFactory.getLogger(CommitListBuilder.class);

    private final GitHubService gitHubService;
    private final CommitParser commitParser;


    @Autowired
    public CommitListBuilder(GitHubService gitHubService, CommitParser commitParser) {
        this.gitHubService = gitHubService;
        this.commitParser = commitParser;
    }


    /**
     * A method which builds a detailed list of commits from a JSON object which only contains vague information about the commits
     *
     * @param repoURI               The repository currently being analysed.
     * @param accessToken           The access token which was provided by GitHub
     * @param vagueCommitsOuterJson A JSON object containing vague information about all of the commits in a repository
     * @return A list of Commit objects which contain detailed information about the commits which were gathered from the
     * repository
     */
    public List<Commit> buildCommitList(String repoURI, String accessToken, JSONObject vagueCommitsOuterJson) {
        List<CompletableFuture<JSONObject>> jsonFutureList = Lists.newArrayList();
        try {
            List<String> shas = extractsSHAs(vagueCommitsOuterJson);
            for (String sha : shas) {
                CompletableFuture<JSONObject> currentJsonFuture = gitHubService.retrieveSpecificCommitAsync(repoURI, sha, accessToken);
                jsonFutureList.add(currentJsonFuture);
            }
        } catch (Exception e) {
            logger.error("Failed to build commit list");
        }

        return commitParser.parseListOfCommits(jsonFutureList);
    }

    List<String> extractsSHAs(JSONObject vagueCommitsOuterJson) {
        List<String> shaList = Lists.newArrayList();
        JSONArray jsonArray = JSONUtils.retrieveJsonArrayOrEmpty(vagueCommitsOuterJson, "commits");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject currentJson = jsonArray.getJSONObject(i);
            shaList.add(JSONUtils.retrieveStringOrEmpty(currentJson, "sha"));
        }
        return shaList;
    }
}
