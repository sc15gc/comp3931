package diss.api;

import com.google.common.collect.Lists;
import diss.parser.AccessTokenParser;
import diss.parser.NameParser;
import diss.parser.RepoNameParser;
import diss.utils.JSONUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class which interacts with the GitHub API
 */
@Service
public class GitHubService {

    private final String GITHUB_BASE = getGitHubApiBase();

    private static final Logger LOG = LoggerFactory.getLogger(GitHubService.class);

    private AccessTokenParser accessTokenParser;
    private RepoNameParser repoNameParser;
    private NameParser nameParser;

    @Autowired
    public GitHubService(AccessTokenParser accessTokenParser, RepoNameParser repoNameParser, NameParser nameParser) {
        this.accessTokenParser = accessTokenParser;
        this.repoNameParser = repoNameParser;
        this.nameParser = nameParser;
    }

    /**
     * A method to retrieve the general information about a repository from the GitHub API
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object including the general information about the repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveRepoJsonAsync(String repoURI, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving generic repo info of %s", repoURI));
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String response = executeGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "?access_token=" + accessToken);
        LOG.info(String.format("Finished retrieving generic repo info of %s", repoURI));
        return CompletableFuture.completedFuture(new JSONObject(response));
    }


    /**
     * A method to retrieve information about a specific page of commits. As the amount retrieved at one time and also
     * the page of commits can be specified, parameters can be passed in here to provide this information.
     *
     * @param repoURI       a link to the repository in question.
     * @param page          Which page of the api information is going to be requested
     * @param accessToken   The VCS access token
     * @param amountPerPage The amount of items to be requested per page..
     * @return A CompletedFuture JSON object including information about the commits on the page specified.
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrievePageOfCommitsAsync(String repoURI, int page, String accessToken, int amountPerPage) throws IOException {
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String response = executeGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "/commits?page=" + page + "&per_page=" + amountPerPage + "&access_token=" + accessToken);
        JSONObject commits = new JSONObject();
        commits.put("commits", new JSONArray(response));
        return CompletableFuture.completedFuture(commits);
    }

    /**
     * A method to retrieve the very first commit in a GitHub repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object with detailed information regarding the first commit in a repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveInitialCommitAsync(String repoURI, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving initial page of commit from %s", repoURI));
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        HttpClient httpClient = HttpClients.createDefault();

        HttpGet httpGet = new HttpGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "/commits?per_page=1" + "&access_token=" + accessToken);
        HttpResponse response = httpClient.execute(httpGet);
        Header[] linkHeader = response.getHeaders("Link");
        String link;
        if (linkHeader.length == 0) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            JSONArray commitArray = new JSONArray(out.toString());
            JSONObject initialCommit = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(commitArray, 0);
            return CompletableFuture.completedFuture(initialCommit);
        } else {
            link = extractLinkToLastPageFromResponse(response);
        }
        JSONArray commitArray = new JSONArray(executeGet(link));
        JSONObject initialCommit = JSONUtils.retrieveJsonObjectFromJsonArrayOrEmpty(commitArray, 0);
        LOG.info(String.format("Finished retrieving initial page of commit from %s", repoURI));
        return CompletableFuture.completedFuture(initialCommit);
    }

    /**
     * A method to retrieve the detailed information about a specific commit
     *
     * @param repoURI     a link to the repository in question.
     * @param sha         The sha identifier of the commit which is going to be retrieved
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object with detailed information about the specified commit
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveSpecificCommitAsync(String repoURI, String sha, String accessToken) throws IOException {
        LOG.info(String.format("retrieving commit %s from repoURI %s", sha, repoURI));
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);

        String response = executeGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "/commits/" + sha + "?access_token=" + accessToken);
        return CompletableFuture.completedFuture(new JSONObject(response));

    }

    /**
     * A method to retrieve the contents of a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object listing all of the files contained in the upper level of the repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveContentsOfRepoAsync(String repoURI, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving contents of repo %s", repoURI));
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String response = executeGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "/contents?access_token=" + accessToken);
        JSONObject contents = new JSONObject();
        contents.put("contents", new JSONArray(response));
        LOG.info(String.format("Finished retrieving contents of repo %s", repoURI));

        return CompletableFuture.completedFuture(contents);
    }


    /**
     * A method to retrieve the repositories which lie within an organisation
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object including the general information about the repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveAllRepositoriesInOrganisation(String repoURI, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving repositories in organisation %s", repoURI));
        HttpClient httpClient = HttpClients.createDefault();
        String organisationName = nameParser.parse(repoURI);

        String url = GITHUB_BASE + "/orgs/" + organisationName
                + "/repos?access_token=" + accessToken + "&per_page=100";

        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet);
        Header[] linkHeader = response.getHeaders("Link");
        int maxPage;
        if (linkHeader.length == 0) {
            maxPage = 1;
        } else {
            String headerValue = (response.getHeaders("Link")[0].getElements()[1].getValue());
            maxPage = getPageNumberFromValue(headerValue);
        }

        List<JSONArray> pages = Lists.newArrayList();
        for (int i = 1; i <= maxPage; i++) {
            httpGet = new HttpGet(url + "&page=" + i);
            httpClient = HttpClients.createDefault();
            response = httpClient.execute(httpGet);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            pages.add(new JSONArray(out.toString()));
        }

        JSONArray allContributors = JSONUtils.stitchArrays(pages);

        JSONObject returnJson = new JSONObject();
        returnJson.put("repositories", allContributors);

        LOG.info(String.format("Finished retrieving repositories in organisation %s", repoURI));

        return CompletableFuture.completedFuture(returnJson);
    }


    /**
     * A method to retrieve the information about the owner of a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture JSON object containing information about the owner of the specified repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<JSONObject> retrieveRepoOwnerInfo(String repoURI, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving owner info of repo %s", repoURI));
        String user = nameParser.parse(repoURI);
        return retrieveUser(user, accessToken);
    }

    /**
     * A method to extract general information about a user
     *
     * @param username    A GitHub username
     * @param accessToken An access token provided by GitHub
     * @return A JSON object giving detail about the user
     * @throws IOException
     */
    @Async
    public CompletableFuture<JSONObject> retrieveUser(String username, String accessToken) throws IOException {
        LOG.info(String.format("Retrieving user info %s", username));
        String response = executeGet(GITHUB_BASE + "/users/" + username + "?access_token=" + accessToken);
        JSONObject userJson = new JSONObject(response);
        return CompletableFuture.completedFuture(userJson);
    }

    /**
     * A method to perform a GET request to the specified URL asynchronously
     *
     * @param url a hyperlink
     * @return A CompletedFuture String of the response from sending a GET request to the URL specified.
     * @throws IOException
     */

    @Async
    public CompletableFuture<String> retrieveFromUrl(String url) throws IOException {
        LOG.info(String.format("Retrieving contents from url: %s", url));
        return CompletableFuture.completedFuture(executeGet(url));
    }


    /**
     * A method to convert raw markdown to HTML
     *
     * @param text        Raw markdown text
     * @param accessToken The VCS access token
     * @return A CompletedFuture String of the HTML representation of the raw markdown
     * @throws IOException
     */

    @Async
    public CompletableFuture<String> convertToMarkdown(String text, String accessToken) throws IOException {
        JSONObject postData = new JSONObject();
        postData.put("text", text);
        postData.put("mode", "gfm");

        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(getGitHubApiBase() + "/markdown?access_token=" + accessToken);
        StringEntity params = new StringEntity(postData.toString());
        httpPost.addHeader("content-type", "application/x-www-form-urlencoded");
        httpPost.setEntity(params);

        HttpResponse response = httpClient.execute(httpPost);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.getEntity().writeTo(out);
        String responseString = out.toString();

        return CompletableFuture.completedFuture(responseString);
    }

    static String getGitHubApiBase() {
        return "https://api.github.com";
    }

    public static String getGitHubBase() {
        return "https://github.com";
    }

    /**
     * A method to retrieve the amount of contributors to a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture Integer of the amount of contributors to the repository specified
     * @throws IOException
     */

    @Async
    public CompletableFuture<Integer> retrieveAmountOfContributors(String repoURI, String accessToken) throws IOException {
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String url = GITHUB_BASE + "/repos/" + user + '/' + repo + "/contributors?page=" +
                "&per_page=100" + "&access_token=" + accessToken;
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet);
        String linkToLastPage = extractLinkToLastPageFromResponse(response);
        if (linkToLastPage.isEmpty()) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            JSONArray singularPage = new JSONArray(out.toString());
            return CompletableFuture.completedFuture(singularPage.length());
        }
        int lastPageNumber = getPageNumberFromValue(linkToLastPage);
        String responseString = executeGet(linkToLastPage);
        JSONArray initialPage = new JSONArray(responseString);
        int numberOfElementsOnLastPage = initialPage.length();
        return CompletableFuture.completedFuture(numberOfElementsOnLastPage + (100 * (lastPageNumber - 1)));

    }

    /**
     * A method to retrieve the amount of branches in a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture Integer object of the amount of branches active in the repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<Integer> retrieveAmountOfBranches(String repoURI, String accessToken) throws IOException {
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String url = GITHUB_BASE + "/repos/" + user + '/' + repo + "/branches?per_page=1" + "&access_token=" + accessToken;

        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet);
        String linkToLastPage = extractLinkToLastPageFromResponse(response);

        if (linkToLastPage.isEmpty()) {
            return CompletableFuture.completedFuture(1);
        }
        int lastPageNumber = getPageNumberFromValue(linkToLastPage);
        return CompletableFuture.completedFuture(lastPageNumber);
    }

    /**
     * A method to retrieve the amount of tags in a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @return A CompletedFuture Integer object signifying the amount of tags in the repository
     * @throws IOException
     */

    @Async
    public CompletableFuture<Integer> retrieveAmountOfTags(String repoURI, String accessToken) throws IOException {
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String url = GITHUB_BASE + "/repos/" + user + '/' + repo + "/tags?per_page=1" + "&access_token=" + accessToken;

        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet);
        String linkToLastPage = extractLinkToLastPageFromResponse(response);

        if (linkToLastPage.isEmpty()) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            JSONArray singularPage = new JSONArray(out.toString());

            return singularPage.length() == 0 ? CompletableFuture.completedFuture(0) : CompletableFuture.completedFuture(1);
        }

        int lastPageNumber = getPageNumberFromValue(linkToLastPage);
        return CompletableFuture.completedFuture(lastPageNumber);
    }


    /**
     * A method to use GitHub to compare two commits in a repository
     *
     * @param repoURI     a link to the repository in question.
     * @param accessToken The VCS access token
     * @param firstSha    the sha of one of the commits to be compared
     * @param secondSha   the sha of one of the commits to be compared
     * @return A CompletedFuture Integer object signifying the amount of tags in the repository
     * @throws IOException
     */

    public JSONObject compareTwoCommits(String repoURI, String accessToken, String firstSha, String secondSha) throws IOException {
        String user = nameParser.parse(repoURI);
        String repo = repoNameParser.parseRepoNameFromUri(repoURI);
        String response = executeGet(GITHUB_BASE + "/repos/" + user + '/' + repo + "/compare/" + firstSha + "..."
                + secondSha + "?access_token=" + accessToken);

        return new JSONObject(response);
    }

    /**
     * A method to retrieve the retrieve the access token from GitHub
     *
     * @param code         A code which GitHub provides at the point of login
     * @param clientId     The application's client identifier
     * @param clientSecret The application's client secret
     * @return A String which is the access token for that user
     */

    public String retrieveAccessToken(String code, String clientId, String clientSecret) {
        try {
            HttpClient httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://github.com/login/oauth/access_token");
            httpPost.setEntity(new UrlEncodedFormEntity(Lists.newArrayList(
                    new BasicNameValuePair("client_id", clientId),
                    new BasicNameValuePair("client_secret", clientSecret),
                    new BasicNameValuePair("code", code))));
            HttpResponse response = httpClient.execute(httpPost);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            return accessTokenParser.parseAccessCode(out.toString());
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * A method to complete a GET request
     *
     * @param url the URL from which the get request will be sent to
     * @return A String which is the HTTP response for that url
     * @throws IOException
     */

    private String executeGet(String url) throws IOException {
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response = httpClient.execute(httpGet);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.getEntity().writeTo(out);
        return out.toString();
    }

    /**
     * A method to parse a page number from a url
     *
     * @param value the URL which contains a page number specified within it
     * @return An int which is the page number specified in the url
     */

    static int getPageNumberFromValue(String value) {
        Pattern p = Pattern.compile("(?<!_)page=[0-9]*");
        Matcher m = p.matcher(value);
        m.find();
        String page = m.group();
        return Integer.parseInt(page.replaceAll("[^0-9]", ""));
    }

    /**
     * A method to retrieve the relational link to the last page which is included in a HTTP response
     *
     * @param response an Http Repsonse
     * @return An link to the last page which is specified inside the lasts page of the Http response.
     */

    static String extractLinkToLastPageFromResponse(HttpResponse response) {
        if (response.getHeaders("Link").length == 0) {
            return "";
        }
        String XMLLinkHeader = response.getHeaders("Link")[0].getElements()[1].toString();
        StringTokenizer stringTokenizer = new StringTokenizer(XMLLinkHeader, "<>");
        return stringTokenizer.nextToken();
    }

}
