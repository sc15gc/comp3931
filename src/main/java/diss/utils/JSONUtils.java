package diss.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * A class which contains safe helper routines for extracting and manipulating information from a JSON Object
 */
public class JSONUtils {


    /**
     * A method to combine JSON arrays into one JSON array
     *
     * @param arrays a List of JSON array objects
     * @return All the JSON array objects together as one JSON array
     */
    public static JSONArray stitchArrays(List<JSONArray> arrays) {
        StringBuilder jsonArrayBuilder = new StringBuilder();
        for (int i = 0; i < arrays.size() - 1; i++) {
            jsonArrayBuilder.append(stripBothBrackets(arrays.get(i).toString()));
            jsonArrayBuilder.append(',');
        }
        jsonArrayBuilder.append(stripFirst(arrays.get(arrays.size() - 1).toString()));
        jsonArrayBuilder.insert(0, '[');
        return new JSONArray(jsonArrayBuilder.toString());
    }

    /**
     * A method to strip the first character from a string.
     *
     * @param array A string.
     * @return A string with the first character removed.
     */
    static String stripFirst(String array) {
        StringBuilder stringBuilder = new StringBuilder(array);
        stringBuilder.deleteCharAt(0);
        return stringBuilder.toString();
    }

    /**
     * A method to strip the last character from a string.
     *
     * @param array A string.
     * @return A string with the last character removed.
     */
    static String stripLast(String array) {
        StringBuilder stringBuilder = new StringBuilder(array);
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    /**
     * A method to strip the first and last character from a string.
     *
     * @param array A string.
     * @return A string with the first and last character removed.
     */
    static String stripBothBrackets(String array) {
        return stripLast(stripFirst(array));
    }


    /**
     * A method to retrieve a string from a JSON object
     *
     * @param jsonObject the JSON object containing the information needed
     * @param desired    the key which points to the desired information
     * @return the string which is associated with the key, if not present returns an empty string.
     */
    public static String retrieveStringOrEmpty(JSONObject jsonObject, String desired) {
        try {
            return jsonObject.getString(desired);
        } catch (JSONException e) {
            return "";
        }
    }

    /**
     * A method to retrieve a boolean from a JSON object
     *
     * @param jsonObject the JSON object containing the information needed
     * @param desired    the key which points to the desired information
     * @return the boolean which is associated with the key, if not present returns an empty boolean.
     */

    public static boolean retrieveBooleanOrFalse(JSONObject jsonObject, String desired) {
        try {
            return jsonObject.getBoolean(desired);
        } catch (JSONException e) {
            return false;
        }
    }

    /**
     * A method to retrieve a JSON array from a JSON object
     *
     * @param jsonObject the JSON object containing the information needed
     * @param desired    the key which points to the desired information
     * @return the JSON array which is associated with the key, if not present returns an empty JSON array.
     */

    public static JSONArray retrieveJsonArrayOrEmpty(JSONObject jsonObject, String desired) {
        try {
            return jsonObject.getJSONArray(desired);
        } catch (JSONException e) {
            return new JSONArray();
        }
    }

    /**
     * A method to retrieve a JSON object from a JSON object
     *
     * @param jsonObject the JSON object containing the information needed
     * @param desired    the key which points to the desired information
     * @return the JSON object which is associated with the key, if not present returns an empty JSON object.
     */

    public static JSONObject retrieveJsonObjectOrEmpty(JSONObject jsonObject, String desired) {
        try {
            return jsonObject.getJSONObject(desired);
        } catch (JSONException e) {
            return new JSONObject();
        }
    }


    /**
     * A method to retrieve the JSON Object from a JSON array at the index requested
     *
     * @param jsonArray the JSON array which contains the JSON object required
     * @param index     the index at which we wish to extract the JSON object
     * @return the JSON object if found, an empty JSON object if not found.
     */
    public static JSONObject retrieveJsonObjectFromJsonArrayOrEmpty(JSONArray jsonArray, int index) {
        try {
            return jsonArray.getJSONObject(index);
        } catch (JSONException e) {
            return new JSONObject();
        }
    }

    /**
     * A method to retrieve an int from a JSON object
     *
     * @param jsonObject the JSON object containing the information needed
     * @param desired    the key which points to the desired information
     * @return the int which is associated with the key, if not present returns an empty int.
     */

    public static int retrieveIntOrEmpty(JSONObject jsonObject, String desired) {
        try {
            return jsonObject.getInt(desired);
        } catch (JSONException e) {
            return 0;
        }
    }
}
