package diss.domain;

import diss.api.domain.Commit;
import diss.api.domain.File;
import diss.api.domain.analysis.CommitListAnalysis;
import diss.lint_utils.domain.Suggestion;

import java.util.List;

/**
 * A class which represents an overview of the analytics which were generated when analysing the repository
 */
public class Analysis {

    private int amountOfCommits;
    private String language;
    private String repoName;
    private boolean gitIgnorePresent;
    private boolean circleCiPresent;
    private File readMeFile;
    private boolean ideaFolderPresent;
    private boolean dockerfilePresent;
    private List<Commit> commits;
    private List<Suggestion> lintOutput;
    private CommitListAnalysis commitListAnalysis;
    private String framework;
    private boolean isPrivate;


    public List<Commit> getCommits() {
        return commits;
    }

    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }


    public boolean isGitIgnorePresent() {
        return gitIgnorePresent;
    }

    public void setGitIgnorePresent(boolean gitIgnorePresent) {
        this.gitIgnorePresent = gitIgnorePresent;
    }

    public boolean isReadMePresent() {
        if (readMeFile.getRawUrl() == null) {
            return false;
        }
        return !readMeFile.getRawUrl().isEmpty();
    }

    public int getAmountOfCommits() {
        return amountOfCommits;
    }

    public void setAmountOfCommits(int amountOfCommits) {
        this.amountOfCommits = amountOfCommits;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public CommitListAnalysis getCommitListAnalysis() {
        return commitListAnalysis;
    }

    public void setCommitListAnalysis(CommitListAnalysis commitListAnalysis) {
        this.commitListAnalysis = commitListAnalysis;
    }

    public boolean isIdeaFolderPresent() {
        return ideaFolderPresent;
    }

    public void setIdeaFolderPresent(boolean ideaFolderPresent) {
        this.ideaFolderPresent = ideaFolderPresent;
    }

    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }

    public boolean isDockerfilePresent() {
        return dockerfilePresent;
    }

    public void setDockerfilePresent(boolean dockerfilePresent) {
        this.dockerfilePresent = dockerfilePresent;
    }

    public File getReadMeFile() {
        return readMeFile;
    }

    public void setReadMeFile(File readMeFileLink) {
        this.readMeFile = readMeFileLink;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public List<Suggestion> getLintOutput() {
        return lintOutput;
    }

    public void setLintOutput(List<Suggestion> lintOutput) {
        this.lintOutput = lintOutput;
    }

    public boolean isCircleCiPresent() {
        return circleCiPresent;
    }

    public void setCircleCiPresent(boolean circleCiPresent) {
        this.circleCiPresent = circleCiPresent;
    }
}
