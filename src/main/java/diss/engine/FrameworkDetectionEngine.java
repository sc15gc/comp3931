package diss.engine;

import diss.api.GitHubService;
import diss.api.domain.File;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * A class to detect if the current repository is using any software frameworks
 */
public class FrameworkDetectionEngine {

    private GitHubService gitHubService;

    @Autowired
    public FrameworkDetectionEngine(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
    }

    /**
     * @param fileList List of files in the repository
     * @return A string containing the framework (if any) which the repository is using
     */
    public String detectFramework(List<File> fileList) {
        for (File file : fileList) {

            switch (file.getFilename()) {
                case "manage.py":
                    return "Django";

                case "pom.xml":
                    return getFrameworkFromBuildFile(file.getRawUrl());

                case "build.gradle":
                    return getFrameworkFromBuildFile(file.getRawUrl());

                case "composer.json":
                    return getFrameworkFromBuildFile(file.getRawUrl());
            }

            if (file.getFilename().equals("manage.py")) {
                return "Django";
            }
        }
        return "Unknown Framework";
    }


    private String getFrameworkFromBuildFile(String rawUrl) {
        try {
            CompletableFuture<String> rawFileFuture = gitHubService.retrieveFromUrl(rawUrl);
            String rawFile = rawFileFuture.get(10000, TimeUnit.MILLISECONDS);
            if (rawFile.toLowerCase().contains("org.springframework")) {
                return "Spring";
            } else if (rawFile.toLowerCase().contains("laravel/laravel")) {
                return "Laravel";
            } else if (rawFile.toLowerCase().contains("symfony")) {
                return "Symfony";
            }
        } catch (Exception e) {
            return "Couldn't retrieve framework";
        }
        return "Unknown Framework";
    }

}
