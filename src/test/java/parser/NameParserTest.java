package parser;


import diss.parser.NameParser;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class NameParserTest {

    private String exampleURI;

    private NameParser nameParser = new NameParser();

    @Test
    void ensureUserParserParsesRepo() {
        exampleURI = "https://github.com/user/repo";
        assertThat(nameParser.parse(exampleURI)).isEqualTo("user");
    }

    @Test
    void ensureUserParserParsesRepoWithLowerCase() {
        exampleURI = "https://github.com/user/repo";
        assertThat(nameParser.parse(exampleURI)).isEqualTo("user");
    }

    @Test
    void ensureUserParserParsesRepoWithoutHttpPrefix() {
        exampleURI = "diss.api.GitHub.com/user/repo";
        assertThat(nameParser.parse(exampleURI)).isEqualTo("user");
    }

    @Test
    void ensureUserParserParsesRepoWithWWW() {
        exampleURI = "www.diss.api.GitHub.com/user/repo";
        assertThat(nameParser.parse(exampleURI)).isEqualTo("user");
    }

}