package parser;

import diss.parser.RepoNameParser;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RepoNameParserTest {

    private String exampleURI;
    private RepoNameParser repoNameParser = new RepoNameParser();

    @Test
    void ensureRepoNameParserParsesRepo() {
        exampleURI = "https://github.com/user/repo";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("repo");
    }

    @Test
    void ensureRepoNameParserParsesRepoWithLowerCase() {
        exampleURI = "https://github.com/user/repo";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("repo");
    }

    @Test
    void ensureRepoNameParserParsesRepoWithoutHttpPrefix() {
        exampleURI = "diss.api.GitHub.com/user/repo";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("repo");
    }

    @Test
    void ensureRepoNameParserParsesRepoWithWWW() {
        exampleURI = "www.diss.api.GitHub.com/user/repo";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("repo");
    }

    @Test
    void returnsEmptyWhenThereIsNoRepoWithoutTrailingSlash() {
        exampleURI = "www.diss.api.GitHub.com/user";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("");
    }

    @Test
    void returnsEmptyWhenThereIsNoRepoWithTrailingSlash() {
        exampleURI = "www.diss.api.GitHub.com/user";
        assertThat(repoNameParser.parseRepoNameFromUri(exampleURI)).isEqualTo("");
    }
}