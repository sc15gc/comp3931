package parser;

import diss.parser.RepositoryDetailsParser;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class RepositoryDetailsParserTest {

    private static JSONObject exampleRepo;
    private static RepositoryDetailsParser repositoryDetailsParser;

    @BeforeAll
    static void setUp() {
        exampleRepo = new JSONObject();
        exampleRepo.put("language", "python");
        exampleRepo.put("name", "reponame");
        exampleRepo.put("private", true);
        repositoryDetailsParser = new RepositoryDetailsParser();
    }

    @Test
    void parsesLanguageFromRepoJson() {
        assertThat(repositoryDetailsParser.parseLanguage(exampleRepo)).isEqualTo("python");
    }

    @Test
    void parsesRepoName() {
        assertThat(repositoryDetailsParser.parseRepoName(exampleRepo)).isEqualTo("reponame");
    }

    @Test
    void parsesPrivateRepoSetting() {
        assertThat(repositoryDetailsParser.parsePrivacySetting(exampleRepo)).isTrue();
    }


}