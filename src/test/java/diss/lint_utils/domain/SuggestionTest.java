package diss.lint_utils.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SuggestionTest {

    @Test
    void returnsGitHubTreeBlobLink() {
        String originalFilePath = "src/main/GitHubService.java";
        Suggestion suggestion = new Suggestion(originalFilePath, "Unneeded Parenthesis", "https://github.com/sc15gc/firstTest", "119");
        assertThat(suggestion.getGitHubLinkToFileOnTreeMaster()).isEqualTo("https://github.com/sc15gc/firstTest/blob/master/src/main/GitHubService.java#L119");
    }

}