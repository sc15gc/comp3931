package diss.lint_utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PmdAnalysisUtilTest {

    private PmdAnalysisUtil pmdAnalysisUtil = new PmdAnalysisUtil();


    @Test
    void buildsOAuthURL() {
        assertThat(pmdAnalysisUtil.buildOAuthUrl("https://github.com/django/django", "ABCD1234"))
                .isEqualTo("https://ABCD1234@github.com/django/django");

    }

    @Test
    void generatesRandomString() {
        String firstRandom = pmdAnalysisUtil.generateRandomString(4);
        String secondRandom = pmdAnalysisUtil.generateRandomString(4);
        String thirdRandom = pmdAnalysisUtil.generateRandomString(4);

        assertThat(firstRandom).isNotEqualTo(secondRandom).isNotEqualTo(thirdRandom);
    }

    @Test
    void parsesFilePathFromSuggestion() {
        String suggestionText = PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/repo_name/GitHubService"
                + ".java:107: Useless parentheses.\"";

        assertThat(pmdAnalysisUtil.parseFilePathFromSuggestion(suggestionText)).isEqualTo("/GitHubService.java");
    }

    @Test
    void parsesFilePathFromSuggestionWithPrecedingFilePathToRepoStore() {
        String suggestionText = "/home/george/Documents/Leeds/year4/modules" + PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/repo_name/GitHubService"
                + ".java:107: Useless parentheses.\"";

        assertThat(pmdAnalysisUtil.parseFilePathFromSuggestion(suggestionText)).isEqualTo("/GitHubService.java");
    }

    @Test
    void parsesFilePathWithUnusualChars() {
        String suggestionText = PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/repo-George-294" +
                "/src_1_delta/GitHubService"
                + ".java:107:\t\n Useless parentheses.\"";

        assertThat(pmdAnalysisUtil.parseFilePathFromSuggestion(suggestionText)).isEqualTo("/src_1_delta/GitHubService.java");
    }

    @Test
    void parsesSuggestionText() {
        String suggestionText = PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/src/GitHubService"
                + ".java:107: Useless parentheses.";

        assertThat(pmdAnalysisUtil.parseFileAndSuggestionText(suggestionText))
                .isEqualTo("GitHubService.java:107: Useless parentheses.");
    }

    @Test
    void parsesSuggestionTextRaw() {
        String suggestionText = PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/src/GitHubService"
                + ".java:107: Useless parentheses.";

        assertThat(pmdAnalysisUtil.parseSuggestionRaw(suggestionText))
                .isEqualTo(" Useless parentheses.");
    }

    @Test
    void parsesSuggestionLineLocation() {
        String suggestionText = PmdAnalysisUtil.retrieveRepoStoreLocation() + "/TYIR/src/GitHubService"
                + ".java:107: Useless parentheses.";

        assertThat(pmdAnalysisUtil.parseLineNumberOfSuggestion(suggestionText))
                .isEqualTo("107");
    }


}