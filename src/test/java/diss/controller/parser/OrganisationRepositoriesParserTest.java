package diss.controller.parser;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrganisationRepositoriesParserTest {

    private OrganisationRepositoriesParser parser = new OrganisationRepositoriesParser();
    private JSONObject organisationOuterJson;

    @BeforeEach
    void setUp() {
        organisationOuterJson = new JSONObject();
        JSONArray repoArray = new JSONArray();
        JSONObject firstRepo = new JSONObject();
        firstRepo.put("html_url", "https://github.com/adobe/brackets-app");
        firstRepo.put("full_name", "adobe/brackets-app");
        JSONObject innerOwnerJson = new JSONObject();
        innerOwnerJson.put("login", "adobe");
        repoArray.put(firstRepo);
        organisationOuterJson.put("repositories", repoArray);
    }

    @Test
    void parsesOrganisationRepositoryWhenCodeLinterIsOn() {
        assertThat(parser.parseOrganisationRepositories(organisationOuterJson, "ACCESS_TOKEN", true).get(0).getAnalysisLink())
                .isEqualTo("/getRepoAnalysis?repoURI=https://github.com/adobe/brackets-app"
                        + "&accessToken=ACCESS_TOKEN"
                        + "&executeCodeLinter=on");
    }

    @Test
    void parsesOrganisationRepositoryWhenCodeLinterIsOff() {
        assertThat(parser.parseOrganisationRepositories(organisationOuterJson, "ACCESS_TOKEN", false).get(0).getAnalysisLink())
                .isEqualTo("/getRepoAnalysis?repoURI=https://github.com/adobe/brackets-app"
                        + "&accessToken=ACCESS_TOKEN"
                        + "&executeCodeLinter=off");
    }


}