package diss.controller.parser;

import diss.controller.domain.OrganisationRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OrganisationRepositoryParserTest {

    private OrganisationRepositoriesParser organisationRepositoriesParser = new OrganisationRepositoriesParser();

    private JSONObject orgReposOuterJson;


    @BeforeEach
    void setUp() {
        orgReposOuterJson = new JSONObject();
        JSONArray orgRepos = new JSONArray();
        orgReposOuterJson.put("repositories", orgRepos);

        JSONObject firstRepo = new JSONObject();
        firstRepo.put("html_url", "https://github.com/user/repo");

        JSONObject owner = new JSONObject();
        firstRepo.put("owner", owner);
        owner.put("login", "organisationName");

        JSONObject secondRepo = new JSONObject();
        secondRepo.put("html_url", "https://github.com/secondUser/secondRepo");

        orgRepos.put(firstRepo);
        orgRepos.put(secondRepo);
    }


    @Test
    void extractsHtmlLinksOfReposInOrgnanisation() {
        List<String> repoList = organisationRepositoriesParser.parseRepoList(orgReposOuterJson);
        assertThat(repoList).hasSize(2);
    }

    @Test
    void extractsNameOfOrganisation() {
        String orgName = organisationRepositoriesParser.parseOrganisationName(orgReposOuterJson);
        assertThat(orgName).isEqualTo("organisationName");
    }

    @Test
    void extractsAllOrganisationRepos() {
        List<OrganisationRepository> organisationRepositories = organisationRepositoriesParser
                .parseOrganisationRepositories(orgReposOuterJson, "access_token", true);
        assertThat(organisationRepositories).hasSize(2);
    }

}