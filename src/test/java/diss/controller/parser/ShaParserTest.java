package diss.controller.parser;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ShaParserTest {

    ShaParser shaParser = new ShaParser();

    @Test
    void extractsLastShaOnPage() {
        JSONObject commits = new JSONObject();
        JSONArray commitArray = new JSONArray();

        JSONObject firstCommit = new JSONObject();
        JSONObject secondCommit = new JSONObject();
        JSONObject thirdCommit = new JSONObject();
        JSONObject fourthCommit = new JSONObject();

        firstCommit.put("sha", "SHA1");
        secondCommit.put("sha", "SHA2");
        thirdCommit.put("sha", "SHA3");
        fourthCommit.put("sha", "SHA4");

        commitArray.put(fourthCommit);
        commitArray.put(thirdCommit);
        commitArray.put(secondCommit);
        commitArray.put(firstCommit);

        commits.put("commits", commitArray);

        assertThat(shaParser.extractLastShaOnPage(commits)).isEqualTo("SHA4");
    }

    @Test
    void extractsShaFromPage() {
        JSONObject commit = new JSONObject();
        commit.put("sha", "SHA1");
        assertThat(shaParser.extractShaFromCommit(commit)).isEqualTo("SHA1");
    }

}