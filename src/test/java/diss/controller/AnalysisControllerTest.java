package diss.controller;

import com.google.common.collect.Lists;
import diss.ApplicationConfiguration;
import diss.api.GitHubService;
import diss.api.domain.Commit;
import diss.api.domain.File;
import diss.api.domain.User;
import diss.api.domain.analysis.CommitListAnalysis;
import diss.api.parser.CommitParser;
import diss.builder.AnalysisBuilder;
import diss.controller.domain.OrganisationRepository;
import diss.controller.parser.OrganisationRepositoriesParser;
import diss.controller.parser.ShaParser;
import diss.domain.Analysis;
import diss.lint_utils.domain.Suggestion;
import diss.parser.RepoNameParser;
import diss.parser.UserParser;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;


@ExtendWith(SpringExtension.class)
@WebMvcTest({ApplicationConfiguration.class})
class AnalysisControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GitHubService gitHubService;

    @MockBean
    private AnalysisBuilder analysisBuilder;

    @MockBean
    private ShaParser shaParser;

    @MockBean
    private RepoNameParser repoNameParser;

    @MockBean
    private UserParser userParser;

    @MockBean
    private OrganisationRepositoriesParser organisationRepositoriesParser;

    @MockBean
    private CommitParser commitParser;

    private List<Commit> commits;
    private List<Suggestion> lintOutput;
    private CommitListAnalysis commitListAnalysis;

    private final String REPO_URI = "https://github.com/organisation";
    private final String ACCESS_TOKEN = "example access token";
    private Analysis analysis;
    private JSONObject repositoryDetails;
    private JSONObject commitsJson;
    private JSONObject contents;
    private JSONObject comparison;
    private User user;
    private Commit initialCommit;

    @BeforeEach
    void setUpNew() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        analysis = mock(Analysis.class);
        commits = Lists.newArrayList();
        commitListAnalysis = new CommitListAnalysis();
        initialCommit = new Commit();
        initialCommit.setCommitterName("John Smith");
        initialCommit.setCommitterEmailAddress("jsmith@email.com");
        initialCommit.setSha("SHA1");
        initialCommit.setCommitterLogin("jsmith");

        Commit longCommit = new Commit();
        longCommit.setMessage("Long message for commit");
        Commit shortCommit = new Commit();
        shortCommit.setMessage("x");
        commitListAnalysis.setMaxCommitLengthMessage(longCommit);
        commitListAnalysis.setMinCommitLengthMessage(shortCommit);
        lintOutput = Lists.newArrayList();
        when(analysis.getAmountOfCommits()).thenReturn(1234);
        when(analysis.getCommits()).thenReturn(commits);
        when(analysis.getLanguage()).thenReturn("python");
        when(analysis.isGitIgnorePresent()).thenReturn(true);
        when(analysis.isCircleCiPresent()).thenReturn(true);
        when(analysis.isDockerfilePresent()).thenReturn(true);
        when(analysis.isReadMePresent()).thenReturn(true);
        when(analysis.isPrivate()).thenReturn(true);
        when(analysis.getLintOutput()).thenReturn(lintOutput);

        File readme = new File();
        readme.setFilename("readme.md");
        readme.setRawUrl("download url");

        when(analysis.getReadMeFile()).thenReturn(readme);
        when(analysis.isIdeaFolderPresent()).thenReturn(true);
        when(analysis.getFramework()).thenReturn("Spring");
        when(analysis.getCommitListAnalysis()).thenReturn(commitListAnalysis);

        repositoryDetails = new JSONObject();
        contents = new JSONObject();
        commitsJson = new JSONObject();
        comparison = new JSONObject();
        JSONObject userInfo = new JSONObject();
        JSONObject initialCommitJson = new JSONObject();
        JSONObject detailedInitialCommitJson = new JSONObject();
        JSONObject initialCommitterJson = new JSONObject();

        CompletableFuture<JSONObject> repositoryDetailsJsonFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> contentsJsonFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> commitsJsonFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> initialCommitJsonFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> userInfoJsonFuture = mock(CompletableFuture.class);
        CompletableFuture<String> readMeFileStringFuture = mock(CompletableFuture.class);
        CompletableFuture<String> readMeFileMarkdownFuture = mock(CompletableFuture.class);
        CompletableFuture<Integer> amountOfContributorsFuture = mock(CompletableFuture.class);
        CompletableFuture<Integer> amountOfBranchesFuture = mock(CompletableFuture.class);
        CompletableFuture<Integer> amountOfTagsFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> detailedInitialCommitFuture = mock(CompletableFuture.class);
        CompletableFuture<JSONObject> initialCommitterJsonFuture = mock(CompletableFuture.class);


        when(repositoryDetailsJsonFuture.getNow(any())).thenReturn(repositoryDetails);
        when(contentsJsonFuture.getNow(any())).thenReturn(contents);
        when(commitsJsonFuture.getNow(any())).thenReturn(commitsJson);
        when(userInfoJsonFuture.getNow(any())).thenReturn(userInfo);
        when(initialCommitJsonFuture.getNow(any())).thenReturn(initialCommitJson);
        when(amountOfContributorsFuture.getNow(any())).thenReturn(10);
        when(amountOfBranchesFuture.getNow(any())).thenReturn(3);
        when(amountOfTagsFuture.getNow(any())).thenReturn(15);


        when(initialCommitJsonFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn(initialCommitJson);
        when(commitsJsonFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn(commitsJson);
        when(readMeFileStringFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn("read me file string");
        when(readMeFileMarkdownFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn("<html> read me file string </html>");

        when(gitHubService.retrieveRepoJsonAsync(REPO_URI, ACCESS_TOKEN)).thenReturn(repositoryDetailsJsonFuture);
        when(gitHubService.retrieveContentsOfRepoAsync(REPO_URI, ACCESS_TOKEN)).thenReturn(contentsJsonFuture);
        when(gitHubService.retrievePageOfCommitsAsync(REPO_URI, 1, ACCESS_TOKEN, 100)).thenReturn(commitsJsonFuture);
        when(gitHubService.retrieveAmountOfContributors(REPO_URI, ACCESS_TOKEN)).thenReturn(amountOfContributorsFuture);
        when(gitHubService.retrieveAmountOfBranches(REPO_URI, ACCESS_TOKEN)).thenReturn(amountOfBranchesFuture);
        when(gitHubService.retrieveAmountOfTags(REPO_URI, ACCESS_TOKEN)).thenReturn(amountOfTagsFuture);
        when(gitHubService.retrieveRepoOwnerInfo(REPO_URI, ACCESS_TOKEN)).thenReturn(userInfoJsonFuture);
        when(gitHubService.retrieveSpecificCommitAsync(REPO_URI, "SHA1", ACCESS_TOKEN)).thenReturn(detailedInitialCommitFuture);

        when(shaParser.extractLastShaOnPage(commitsJson)).thenReturn("SHA8");
        when(shaParser.extractShaFromCommit(initialCommitJson)).thenReturn("SHA1");

        when(detailedInitialCommitFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn(detailedInitialCommitJson);
        when(initialCommitterJsonFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn(initialCommitterJson);


        when(commitParser.parseCommit(detailedInitialCommitJson)).thenReturn(initialCommit);

        user = new User();
        user.setName("jsmith");

        when(userParser.parseUser(any())).thenReturn(user);

        when(gitHubService.retrieveInitialCommitAsync(REPO_URI, ACCESS_TOKEN)).thenReturn(initialCommitJsonFuture);
        when(gitHubService.retrievePageOfCommitsAsync(REPO_URI, 1, ACCESS_TOKEN, 100)).thenReturn(commitsJsonFuture);

        when(gitHubService.compareTwoCommits(REPO_URI, ACCESS_TOKEN, "SHA1", "SHA8")).thenReturn(comparison);
        when(gitHubService.retrieveFromUrl("download url")).thenReturn(readMeFileStringFuture);
        when(gitHubService.convertToMarkdown("read me file string", ACCESS_TOKEN)).thenReturn(readMeFileMarkdownFuture);
        when(gitHubService.retrieveUser("jsmith", ACCESS_TOKEN)).thenReturn(initialCommitterJsonFuture);

        when(analysisBuilder.buildAnalysis(REPO_URI, ACCESS_TOKEN, repositoryDetails, contents, commitsJson, comparison, true)).thenReturn(analysis);

        when(repoNameParser.parseRepoNameFromUri(REPO_URI)).thenReturn("repo Name");
    }

    @Test
    void controllerSendsCorrectAttributesToView() throws Exception {
        this.mockMvc.perform(get("/getRepoAnalysis?repoURI=" + REPO_URI + "&accessToken=" + ACCESS_TOKEN + "&executeCodeLinter=on"))
                .andExpect(model().attribute("language", "python"))
                .andExpect(model().attribute("repoUri", REPO_URI))
                .andExpect(model().attribute("amountOfCommits", 1234))
                .andExpect(model().attribute("readMePresent", true))
                .andExpect(model().attribute("readMeFile", "<html> read me file string </html>"))
                .andExpect(model().attribute("gitIgnorePresent", true))
                .andExpect(model().attribute("circleCiPresent", true))
                .andExpect(model().attribute("dockerfilePresent", true))
                .andExpect(model().attribute("commits", commits))
                .andExpect(model().attribute("framework", "Spring"))
                .andExpect(model().attribute("lintOutput", lintOutput))
                .andExpect(model().attribute("amountOfContributors", 10))
                .andExpect(model().attribute("amountOfBranches", 3))
                .andExpect(model().attribute("amountOfTags", 15))
                .andExpect(model().attribute("user", user))
                .andExpect(model().attribute("initialCommit", initialCommit))
                .andExpect(model().attribute("initialCommitter", user))
                .andExpect(model().attribute("commitAnalytics", commitListAnalysis));
    }

    @Test
    void checksLintOutputIsNotPresentWhenCheckboxIsFalse() throws Exception {
        when(analysisBuilder.buildAnalysis(REPO_URI, ACCESS_TOKEN, repositoryDetails, contents, commitsJson, comparison, false)).thenReturn(analysis);
        this.mockMvc.perform(get("/getRepoAnalysis?repoURI=" + REPO_URI + "&accessToken=" + ACCESS_TOKEN + "&executeCodeLinter=off"))
                .andExpect(model().attribute("lintOutput", equalTo(Lists.newArrayList())));
    }

    @Test
    void controllerSendsCorrectAttributesToViewDifferentURI() throws Exception {
        this.mockMvc.perform(get("/getRepoAnalysis?repoURI=" + REPO_URI + "&accessToken=" + ACCESS_TOKEN + "&executeCodeLinter=on"))
                .andExpect(model().attribute("language", "python"))
                .andExpect(model().attribute("amountOfCommits", 1234))
                .andExpect(model().attribute("readMePresent", true))
                .andExpect(model().attribute("readMeFile", "<html> read me file string </html>"))
                .andExpect(model().attribute("isPrivate", true))
                .andExpect(model().attribute("gitIgnorePresent", true))
                .andExpect(model().attribute("dockerfilePresent", true))
                .andExpect(model().attribute("commits", commits))
                .andExpect(model().attribute("framework", "Spring"))
                .andExpect(model().attribute("commitAnalytics", commitListAnalysis));
    }


    @Test
    void controllerBuildsListOfReposWhenOrganisationInput() throws Exception {
        when(repoNameParser.parseRepoNameFromUri(REPO_URI)).thenReturn("");

        CompletableFuture<JSONObject> organisationRepoListJsonFuture = mock(CompletableFuture.class);
        JSONObject organisationRepoList = new JSONObject();

        when(organisationRepoListJsonFuture.get(10000, TimeUnit.MILLISECONDS)).thenReturn(organisationRepoList);

        List<OrganisationRepository> organisationRepositories = Lists.newArrayList();

        when(organisationRepositoriesParser.parseOrganisationRepositories(organisationRepoList, ACCESS_TOKEN, true)).thenReturn(organisationRepositories);
        when(gitHubService.retrieveAllRepositoriesInOrganisation(REPO_URI, ACCESS_TOKEN))
                .thenReturn(organisationRepoListJsonFuture);

        this.mockMvc.perform(get("/analyse?repoURI=" + REPO_URI + "&accessToken=" + ACCESS_TOKEN + "&executeCodeLinter=on"))
                .andExpect(model().attribute("organisationRepositories", organisationRepositories));
    }


}