package diss.controller;


import diss.api.GitHubService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@WebMvcTest({HomepageController.class})
class HomepageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GitHubService gitHubService;

    @Test
    void controllerShouldRenderHomepageAndHaveCodeInTheModel() throws Exception {
        when(gitHubService.retrieveAccessToken(anyString(), anyString(), anyString())).thenReturn("ACCESS_TOKEN");
        this.mockMvc.perform(get("/homepage"))
                .andExpect(view().name("redirect:/login"));
    }

    @Test
    void controllerShouldRedirectEntryIndexToHomepage() throws Exception {
        when(gitHubService.retrieveAccessToken(anyString(), anyString(), anyString())).thenReturn("ACCESS_TOKEN");
        this.mockMvc.perform(get("/"))
                .andExpect(view().name("redirect:/homepage"));
    }

    @Test
    void controllerShouldRedirectToLoginIfAccessTokenNotPresent() throws Exception {
        when(gitHubService.retrieveAccessToken(anyString(), anyString(), anyString())).thenReturn("ACCESS_TOKEN");
        this.mockMvc.perform(get("/homepage?code=1234"))
                .andExpect(model().attribute("accessToken", "ACCESS_TOKEN"))
                .andExpect(view().name("homepage"));
    }


}