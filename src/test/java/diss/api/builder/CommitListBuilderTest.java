package diss.api.builder;

import com.google.common.collect.Lists;
import diss.api.GitHubService;
import diss.api.domain.Commit;
import diss.api.parser.CommitParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class CommitListBuilderTest {

    private final String REPO_URI = "repoUri";
    private final String ACCESS_TOKEN = "ABCD1234";
    private JSONObject outerCommitArrayJson;

    private CommitListBuilder commitListBuilder;


    @BeforeEach
    void setUp() throws IOException, InterruptedException, ExecutionException, TimeoutException {

        GitHubService gitHubService = mock(GitHubService.class);
        CommitParser commitParser = mock(CommitParser.class);

        commitListBuilder = new CommitListBuilder(gitHubService, commitParser);
        outerCommitArrayJson = new JSONObject();
        JSONArray commitArray = new JSONArray();
        outerCommitArrayJson.put("commits", commitArray);


        CompletableFuture firstCompletableJsonFuture = mock(CompletableFuture.class);
        CompletableFuture secondCompletableJsonFuture = mock(CompletableFuture.class);
        CompletableFuture thirdCompletableJsonFuture = mock(CompletableFuture.class);


        JSONObject firstVagueCommitJson = new JSONObject();
        firstVagueCommitJson.put("sha", "first SHA");
        JSONObject secondVagueCommitJson = new JSONObject();
        secondVagueCommitJson.put("sha", "second SHA");
        JSONObject thirdVagueCommitJson = new JSONObject();
        thirdVagueCommitJson.put("sha", "third SHA");

        commitArray.put(firstVagueCommitJson);
        commitArray.put(secondVagueCommitJson);
        commitArray.put(thirdVagueCommitJson);

        JSONObject firstFullCommitJson = new JSONObject();
        firstFullCommitJson.put("node_id", "first node_id");
        JSONObject secondFullCommitJson = new JSONObject();
        secondFullCommitJson.put("node_id", "second node_id");
        JSONObject thirdFullCommitJson = new JSONObject();
        thirdFullCommitJson.put("node_id", "third node_id");


        when(gitHubService.retrieveSpecificCommitAsync(REPO_URI, "first SHA", ACCESS_TOKEN)).thenReturn(firstCompletableJsonFuture);
        when(gitHubService.retrieveSpecificCommitAsync(REPO_URI, "second SHA", ACCESS_TOKEN)).thenReturn(secondCompletableJsonFuture);
        when(gitHubService.retrieveSpecificCommitAsync(REPO_URI, "third SHA", ACCESS_TOKEN)).thenReturn(thirdCompletableJsonFuture);

        Commit firstCommit = new Commit();
        firstCommit.setNodeId("first node_id");
        Commit secondCommit = new Commit();
        secondCommit.setNodeId("second node_id");
        Commit thirdCommit = new Commit();
        thirdCommit.setNodeId("third node_id");


        List<CompletableFuture<JSONObject>> jsonFutures = Lists.newArrayList();
        jsonFutures.add(firstCompletableJsonFuture);
        jsonFutures.add(secondCompletableJsonFuture);
        jsonFutures.add(thirdCompletableJsonFuture);

        when(firstCompletableJsonFuture.get(3000, TimeUnit.MILLISECONDS)).thenReturn(firstVagueCommitJson);
        when(secondCompletableJsonFuture.get(3000, TimeUnit.MILLISECONDS)).thenReturn(secondVagueCommitJson);
        when(thirdCompletableJsonFuture.get(3000, TimeUnit.MILLISECONDS)).thenReturn(thirdVagueCommitJson);

        when(commitParser.parseCommit(firstFullCommitJson)).thenReturn(firstCommit);
        when(commitParser.parseCommit(secondFullCommitJson)).thenReturn(secondCommit);
        when(commitParser.parseCommit(thirdFullCommitJson)).thenReturn(thirdCommit);

        List<Commit> commitList = Lists.newArrayList();
        commitList.add(firstCommit);
        commitList.add(secondCommit);
        commitList.add(thirdCommit);

        when(commitParser.parseListOfCommits(jsonFutures)).thenReturn(commitList);

    }

    @Test
    void returnsExpectedCommits() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        List<Commit> commitList = commitListBuilder.buildCommitList(REPO_URI, ACCESS_TOKEN, outerCommitArrayJson);
        assertThat(commitList.get(0).getNodeId()).isEqualTo("first node_id");
        assertThat(commitList.get(1).getNodeId()).isEqualTo("second node_id");
        assertThat(commitList.get(2).getNodeId()).isEqualTo("third node_id");
    }

    @Test
    void returnsExpectedCommitsAsync() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        List<Commit> commitList = commitListBuilder.buildCommitList(REPO_URI, ACCESS_TOKEN, outerCommitArrayJson);
        assertThat(commitList.get(0).getNodeId()).isEqualTo("first node_id");
        assertThat(commitList.get(1).getNodeId()).isEqualTo("second node_id");
        assertThat(commitList.get(2).getNodeId()).isEqualTo("third node_id");
    }

    @Test
    void ExtractsShasCorrectly() {
        assertThat(commitListBuilder).isNotNull();
        assertThat(commitListBuilder.extractsSHAs(outerCommitArrayJson)).hasSize(3);
    }

}