package diss.api.parser;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ModifiedFilesParserTest {

    private ModifiedFilesParser modifiedFilesParser = new ModifiedFilesParser();

    private JSONObject outerCommitJson = new JSONObject();

    @BeforeEach
    void setUp() {
        JSONArray files = new JSONArray();
        JSONObject firstChangedFile = new JSONObject();
        firstChangedFile.put("filename", "file 1");
        firstChangedFile.put("changes", 5);
        firstChangedFile.put("blob_url", "www.blob url");
        
        JSONObject secondChangedFile = new JSONObject();
        secondChangedFile.put("filename", "file 2");
        secondChangedFile.put("changes", 2);

        files.put(firstChangedFile);
        files.put(secondChangedFile);
        outerCommitJson.put("files", files);
    }


    @Test
    void extractsFiles() {
        assertThat(modifiedFilesParser.parse(outerCommitJson)).hasSize(2);
        assertThat(modifiedFilesParser.parse(outerCommitJson).get(0).getFilename()).isEqualTo("file 1");
    }


    @Test
    void parsesAmountOfChanges() {
        assertThat(modifiedFilesParser.parse(outerCommitJson)).hasSize(2);
        assertThat(modifiedFilesParser.parse(outerCommitJson).get(0).getAmountOfChanges()).isEqualTo(5);
        assertThat(modifiedFilesParser.parse(outerCommitJson).get(1).getAmountOfChanges()).isEqualTo(2);
    }


    @Test
    void parsesBlobUrl() {
        assertThat(modifiedFilesParser.parse(outerCommitJson).get(0).getBlobUrl()).isEqualTo("www.blob url");
    }


}