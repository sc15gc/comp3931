package diss.api.parser;

import com.google.common.collect.Lists;
import diss.api.domain.Commit;
import diss.api.domain.ModifiedFile;
import diss.parser.RepoNameParser;
import diss.parser.NameParser;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CommitParserTest {

    private JSONObject outerCommitJson;
    private CommitParser commitParser;
    private Commit commit;

    @BeforeEach
    void setUp() {
        String htmlUrl = "www.github.com/username/reponame/fdsafads/fsadfasfsda/fads";
        outerCommitJson = new JSONObject();
        outerCommitJson.put("sha", "SHA1");
        outerCommitJson.put("html_url", htmlUrl);
        JSONObject commitJson = new JSONObject();
        outerCommitJson.put("commit", commitJson);
        commitJson.put("message", "message accompanying commit");
        ModifiedFilesParser modifiedFilesParser = mock(ModifiedFilesParser.class);
        NameParser nameParser = mock(NameParser.class);
        RepoNameParser repoNameParser = mock(RepoNameParser.class);
        ModifiedFile modifiedFile = new ModifiedFile();
        modifiedFile.setFilename("firstFile.txt");

        ModifiedFile secondModifiedFile = new ModifiedFile();
        secondModifiedFile.setFilename("secondFile.txt");

        List<ModifiedFile> modifiedFilesList = Lists.newArrayList();
        modifiedFilesList.add(modifiedFile);
        modifiedFilesList.add(secondModifiedFile);

        JSONObject committer = new JSONObject();
        committer.put("date", "2018-11-27T16:20:23Z");

        JSONObject author = new JSONObject();
        author.put("name", "John Smith");
        author.put("email", "jsmith@email.com");
        author.put("login", "jsmith");

        commitJson.put("committer", committer);
        commitJson.put("author", author);

        outerCommitJson.put("author", author);

        when(modifiedFilesParser.parse(outerCommitJson)).thenReturn(modifiedFilesList);
        when(nameParser.parse(htmlUrl)).thenReturn("username");
        when(repoNameParser.parseRepoNameFromUri(htmlUrl)).thenReturn("reponame");
        commitParser = new CommitParser(modifiedFilesParser, nameParser, repoNameParser);
        commit = commitParser.parseCommit(outerCommitJson);
    }

    @Test
    void parsesMessage() {
        assertThat(commit.getMessage())
                .isEqualTo("message accompanying commit");
    }

    @Test
    void parsesModifiedFiles() {
        assertThat(commit.getModifiedFiles()).hasSize(2);
    }

    @Test
    void parsesDate() {
        assertThat(commit.getDate()).isEqualTo("2018-11-27T16:20:23Z");
    }

    @Test
    void parsesSha() {
        assertThat(commit.getSha()).isEqualTo("SHA1");
    }

    @Test
    void parsesCommitDiffUrl() {
        assertThat(commit.getCommitDiffUrl()).isEqualTo("https://github.com/username/reponame/commit/SHA1");
    }

    @Test
    void parsesName() {
        assertThat(commit.getCommitterName()).isEqualTo("John Smith");
    }

    @Test
    void parsesEmail() {
        assertThat(commit.getCommitterEmailAddress()).isEqualTo("jsmith@email.com");
    }

    @Test
    void parsesCommitterLogin() {
        assertThat(commit.getCommitterLogin()).isEqualTo("jsmith");
    }

}