package diss.api;


import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class GitHubServiceTest {

    @Test
    void retrievesGitHubBaseUrl() {
        assertThat(GitHubService.getGitHubApiBase()).isNotNull();
    }

    @Test
    void extractsPageNumber() {
        assertThat(GitHubService.getPageNumberFromValue("page=4&per_page=100>")).isEqualTo(4);
    }

    @Test
    void extractsPageNumberWhenItComesLast() {
        assertThat(GitHubService.getPageNumberFromValue("per_page=100&page=4>")).isEqualTo(4);
    }

    @Test
    void extractsLinkToFinalPageFromResponse() {
        HttpResponse httpResponse = mock(HttpResponse.class);
        Header[] headers = new Header[1];
        Header header = mock(Header.class);
        HeaderElement[] headerElements = new HeaderElement[2];
        HeaderElement headerElement = mock(HeaderElement.class);
        headerElements[1] = headerElement;
        headers[0] = header;

        when(httpResponse.getHeaders("Link")).thenReturn(headers);
        when(header.getElements()).thenReturn(headerElements);
        when(header.getElements()).thenReturn(headerElements);
        when(headerElement.toString())
                .thenReturn("<https://api.github.com/repositories/4164482/commits?page=886&access_token=ACCESS_TOKEN>; rel=last");


        assertThat(GitHubService.
                extractLinkToLastPageFromResponse(httpResponse))
                .isEqualTo("https://api.github.com/repositories/4164482/commits?page=886&access_token=ACCESS_TOKEN");
    }

}