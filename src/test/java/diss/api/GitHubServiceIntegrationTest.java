package diss.api;

import diss.parser.AccessTokenParser;
import diss.parser.NameParser;
import diss.parser.RepoNameParser;
import diss.utils.JSONUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.assertj.core.api.Assertions.assertThat;


class GitHubServiceIntegrationTest {


    private final GitHubService gitHubService = new GitHubService(new AccessTokenParser(), new RepoNameParser(), new NameParser());
    private String ACCESS_TOKEN = "";
    private final String DJANGO_GITHUB = "https://github.com/django/django";
    private final String ADOBE_GITHUB_ORG = "https://github.com/adobe";
    private final String FIRST_TEST = "https://github.com/sc15gc/firstTest";


    @Test
    void retrievesRepoJson() throws IOException {
        CompletableFuture<JSONObject> repoJsonFuture = gitHubService.retrieveRepoJsonAsync(DJANGO_GITHUB, ACCESS_TOKEN);
        assertThat(repoJsonFuture.getNow(null)).isNotNull();
    }

    @Test
    void ensuresGitHubAPIFetchesCommitPage() throws IOException {
        CompletableFuture<JSONObject> commitsJsonFuture =
                gitHubService.retrievePageOfCommitsAsync(DJANGO_GITHUB, 1, ACCESS_TOKEN, 30);
        JSONObject commitsJson = commitsJsonFuture.getNow(null);
        assertThat(commitsJson).isNotNull();
        assertThat(commitsJson.getJSONArray("commits")).hasSize(30);

    }

    @Test
    void retrievesAllOrganisationRepos() throws IOException {
        CompletableFuture<JSONObject> commitsJsonFuture =
                gitHubService.retrieveAllRepositoriesInOrganisation(ADOBE_GITHUB_ORG, ACCESS_TOKEN);
        JSONObject reposJson = commitsJsonFuture.getNow(null);
        assertThat(reposJson).isNotNull();
        assertThat(reposJson.getJSONArray("repositories").length()).isGreaterThan(100);

    }

    @Test
    void retrievesPageOfCommitsSpecifyingOnePerPage() throws IOException {
        CompletableFuture<JSONObject> commitsJsonFuture = gitHubService.retrievePageOfCommitsAsync(FIRST_TEST, 1, ACCESS_TOKEN, 1);
        JSONObject commitsJson = commitsJsonFuture.getNow(null);
        assertThat(commitsJson).isNotNull();
        assertThat(commitsJson.getJSONArray("commits")).hasSize(1);
    }

    @Test
    void retrievesFirstCommitPageWhenThereIsOnlyOne() throws IOException {
        CompletableFuture<JSONObject> commitJsonFuture =
                gitHubService.retrieveInitialCommitAsync(FIRST_TEST, ACCESS_TOKEN);
        JSONObject commitsJson = commitJsonFuture.getNow(null);
        assertThat(commitsJson).isNotNull();
        assertThat(commitsJson.getString("sha")).isEqualTo("cf656349736c4793f87706c5055fb6654b1570bf");
    }

    @Test
    void retrievesSpecificCommit() throws IOException {
        CompletableFuture<JSONObject> commitJsonFuture = gitHubService.retrieveSpecificCommitAsync(DJANGO_GITHUB, "926fa7116fd633b69277c3ad9b3370ca45163231", ACCESS_TOKEN);
        JSONObject commitJson = commitJsonFuture.getNow(null);
        assertThat(commitJson).isNotNull();
        assertThat(commitJson.getString("node_id")).isEqualTo("MDY6Q29tbWl0NDE2NDQ4Mjo5MjZmYTcxMTZmZDYzM2I2OTI3N2MzYWQ5YjMzNzBjYTQ1MTYzMjMx");
    }

    @Test
    void retrievesContents() throws IOException {
        CompletableFuture<JSONObject> contributorsJsonFuture = gitHubService.retrieveContentsOfRepoAsync(DJANGO_GITHUB, ACCESS_TOKEN);
        JSONObject contributorsJson = contributorsJsonFuture.getNow(null);
        assertThat((contributorsJson.getJSONArray("contents").length() > 5)).isTrue();
    }

    @Test
    void retrievesContentsOfSmallRepo() throws IOException {
        CompletableFuture<JSONObject> contributorsJsonFuture = gitHubService.retrieveContentsOfRepoAsync(FIRST_TEST, ACCESS_TOKEN);
        JSONObject contributors = contributorsJsonFuture.getNow(null);
        assertThat((contributors.getJSONArray("contents").length() > 2)).isTrue();
    }

    @Test
    void canCompareTwoCommits() throws IOException {
        JSONObject comparison = gitHubService.compareTwoCommits("https://github.com/sc15gc/firstTest", ACCESS_TOKEN,
                "cf656349736c4793f87706c5055fb6654b1570bf",
                "5e38e91ae832288655e7bef09d41d4979dec00b8");
        assertThat(JSONUtils.retrieveIntOrEmpty(comparison, "ahead_by")).isEqualTo(5);
    }

    @Test
    void canGetSpecificFileFromMaster() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        JSONObject contributorsJson = gitHubService.retrieveContentsOfRepoAsync(FIRST_TEST, ACCESS_TOKEN)
                .get(1000, TimeUnit.MILLISECONDS);
        assertThat((contributorsJson.getJSONArray("contents").length() > 2)).isTrue();

        JSONArray contentsArray = contributorsJson.getJSONArray("contents");

        String firstRawFileUrl = contentsArray.getJSONObject(contentsArray.length() - 1).getString("download_url");

        CompletableFuture<String> rawFileFuture = gitHubService.retrieveFromUrl(firstRawFileUrl);

        String rawFile = rawFileFuture.get(1000, TimeUnit.MILLISECONDS);
        assertThat(rawFile).isNotNull();
        assertThat(rawFile).isNotEmpty();
    }

    @Test
    void convertsTextToMarkDown() throws InterruptedException, ExecutionException, TimeoutException, IOException {

        CompletableFuture<String> markdownTextFuture = gitHubService
                .convertToMarkdown("Hello world #1 **cool**, and #1!", ACCESS_TOKEN);

        String markdownText = markdownTextFuture.get(1000, TimeUnit.MILLISECONDS);
        assertThat(markdownText).isEqualTo("<p>Hello world #1 <strong>cool</strong>, and #1!</p>");
    }

    @Test
    void retrievesAmountOfContributorsWithLargeRepo() throws IOException {
        CompletableFuture<Integer> amountOfContributors = gitHubService.retrieveAmountOfContributors(DJANGO_GITHUB, ACCESS_TOKEN);
        assertThat(amountOfContributors.getNow(null)).isGreaterThan(350);
    }

    @Test
    void retrievesAmountOfContributorsWithSmallRepo() throws IOException {
        CompletableFuture<Integer> amountOfContributors = gitHubService.retrieveAmountOfContributors(FIRST_TEST, ACCESS_TOKEN);
        assertThat(amountOfContributors.getNow(null)).isEqualTo(1);
    }

    @Test
    void retrievesOwnerInfo() throws IOException {
        CompletableFuture<JSONObject> ownerInfo = gitHubService.retrieveRepoOwnerInfo(FIRST_TEST, ACCESS_TOKEN);
        assertThat(ownerInfo.getNow(null).getString("login")).isEqualTo("sc15gc");
    }

    @Test
    void retrievesAmountOfBranchesFromSmallRepo() throws IOException {
        CompletableFuture<Integer> amountOfContributors = gitHubService.retrieveAmountOfBranches(FIRST_TEST, ACCESS_TOKEN);
        assertThat(amountOfContributors.getNow(null)).isEqualTo(1);
    }

    @Test
    void retrievesAmountOfBranchesFromLargeRepo() throws IOException {
        CompletableFuture<Integer> amountOfContributors = gitHubService.retrieveAmountOfBranches(DJANGO_GITHUB, ACCESS_TOKEN);
        assertThat(amountOfContributors.getNow(null)).isGreaterThan(10);
    }

    @Test
    void retrievesAmountOfTags() throws IOException {
        CompletableFuture<Integer> amountOfReleases = gitHubService.retrieveAmountOfTags("https://github.com/pallets/flask", ACCESS_TOKEN);
        assertThat(amountOfReleases.getNow(null)).isGreaterThan(25);
    }

    @Test
    void detectsWhenTagsArrayIsEmpty() throws IOException {
        CompletableFuture<Integer> amountOfReleases = gitHubService.retrieveAmountOfTags("https://github.com/sc15gc/firstTest", ACCESS_TOKEN);
        assertThat(amountOfReleases.getNow(null)).isEqualTo(0);
    }

    @Test
    void retrievesUserInfo() throws IOException {
        CompletableFuture<JSONObject> userInfoJsonFuture = gitHubService.retrieveUser("sc15gc", ACCESS_TOKEN);
        assertThat(userInfoJsonFuture.getNow(null).getString("login")).isEqualTo("sc15gc");

    }

    private String retrieveLocalAccessToken() {
        try {
            Properties apiProperties = new Properties();
            apiProperties.load(new FileInputStream("./src/main/resources/properties/api.properties"));
            return (apiProperties.getProperty("github.api.access_token"));
        } catch (Exception e) {
            return "cannot find github access token";
        }
    }

}