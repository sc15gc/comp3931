package diss.api.domain.analysis;

import com.google.common.collect.Lists;
import diss.api.domain.Commit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class CommitBulkAnalyserTest {

    private CommitBulkAnalyser commitBulkAnalyser = new CommitBulkAnalyser();
    private List<Commit> commits;

    @BeforeEach
    void setUp() {
        commits = Lists.newArrayList();
        Commit firstCommit = new Commit();
        Commit secondCommit = new Commit();
        Commit thirdCommit = new Commit();
        Commit fourthCommit = new Commit();
        Commit fifthCommit = new Commit();
        firstCommit.setMessage("first message");
        firstCommit.setDate(new Date(1));
        secondCommit.setDate(new Date(2));
        thirdCommit.setDate(new Date(3));
        fourthCommit.setDate(new Date(4));
        fifthCommit.setDate(new Date(5));
        secondCommit.setMessage("second message");
        thirdCommit.setMessage("third message");
        fourthCommit.setMessage("fourth message");
        fifthCommit.setMessage("x");
        commits.add(firstCommit);
        commits.add(secondCommit);
        commits.add(thirdCommit);
        commits.add(fourthCommit);
        commits.add(fifthCommit);
    }


    @Test
    void analysisStoresTheAverageLengthOfCommitMessage() {
        assertThat(commitBulkAnalyser.analyse(commits).getAverageLengthOfCommitMessage()).isEqualTo(11);
    }

    @Test
    void analysisStoresCommitMessagesOfEitherExtreme() {
        assertThat(commitBulkAnalyser.analyse(commits).getMaxCommitMessageLengthObject().getMessage()).isEqualTo("second message");
        assertThat(commitBulkAnalyser.analyse(commits).getMinCommitMessageLengthObject().getMessage()).isEqualTo("x");
    }

    @Test
    void retrievesAllCommitMessagesLengths() {
        assertThat(commitBulkAnalyser.analyse(commits).getCommitMessageLengths().size()).isEqualTo(5);
    }

    @Test
    void retrievesRawCommitMessages() {
        assertThat(commitBulkAnalyser.analyse(commits).getRawMessages().get(0)).isEqualTo("first message");
    }

    @Test
    void retrievesDates() {
        assertThat(commitBulkAnalyser.analyse(commits).getDates().get(0)).isEqualTo(1);
        assertThat(commitBulkAnalyser.analyse(commits).getDates()).hasSize(5);
    }


    @Test
    void canCalculateStandardDeviation() {
        List<Integer> commitLengths = Lists.newArrayList();
        commitLengths.add(9);
        commitLengths.add(2);
        commitLengths.add(5);
        commitLengths.add(4);
        commitLengths.add(12);
        commitLengths.add(7);
        commitLengths.add(8);
        commitLengths.add(11);
        commitLengths.add(9);
        commitLengths.add(3);
        commitLengths.add(7);
        commitLengths.add(4);
        commitLengths.add(12);
        commitLengths.add(5);
        commitLengths.add(4);
        commitLengths.add(10);
        commitLengths.add(9);
        commitLengths.add(6);
        commitLengths.add(9);
        commitLengths.add(4);

        int mean = 7;

        double num = (Math.round(commitBulkAnalyser.calculateStandardDeviation(commitLengths, mean) * 100));
        double twoDp = num / 100;

        assertThat(twoDp).isEqualTo(2.98);

    }

}