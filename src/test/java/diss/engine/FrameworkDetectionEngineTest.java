package diss.engine;

import diss.ApplicationConfiguration;
import diss.api.GitHubService;
import diss.api.domain.File;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@WebMvcTest({ApplicationConfiguration.class})
class FrameworkDetectionEngineTest {

    @MockBean
    private GitHubService gitHubService;

    @Autowired
    private FrameworkDetectionEngine frameworkDetectionEngine;

    @Test
    void detectsDjango() {
        List<File> files = Lists.newArrayList();
        File file = new File();
        file.setFilename("manage.py");
        files.add(file);
        String framework = frameworkDetectionEngine.detectFramework(files);

        assertThat(framework).isEqualTo("Django");
    }

    @Test
    void detectsSpringFromPomBuildFile() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        List<File> files = Lists.newArrayList();
        File file = new File();
        file.setFilename("pom.xml");
        file.setRawUrl("rawurl.com");
        files.add(file);
        CompletableFuture<String> completableFutureRawFile = mock(CompletableFuture.class);

        String contentsOfFile = "data \n data \n \t\t<profile>\n" +
                "\t\t\t<id>release</id>\n" +
                "\t\t\t<build>\n" +
                "\t\t\t\t<plugins>\n" +
                "\t\t\t\t\t<plugin>\n" +
                "\t\t\t\t\t\t<groupId>org.jfrog.buildinfo</groupId>\n" +
                "\t\t\t\t\t\t<artifactId>artifactory-maven-plugin</artifactId>\n" +
                "\t\t\t\t\t\t<inherited>false</inherited>\n" +
                "\t\t\t\t\t</plugin>\n" +
                "\t\t\t\t</plugins>\n" +
                "\t\t\t</build>\n" +
                "\t\t</profile>\n" +
                "\t</profiles>\n" +
                "\n" +
                "\t<dependencies>\n" +
                "\n" +
                "\t\t<dependency>\n" +
                "\t\t\t<groupId>${project.groupId}</groupId>\n" +
                "\t\t\t<artifactId>spring-data-commons</artifactId>\n" +
                "\t\t\t<version>${springdata.commons}</version>\n" +
                "\t\t</dependency>\n" +
                "\n" +
                "\t\t<dependency>\n" +
                "\t\t\t<groupId>org.springframework</groupId>\n" +
                "\t\t\t<artifactId>spring-orm</artifactId>\n" +
                "\t\t</dependency>\n";

        when(completableFutureRawFile.get(10000, TimeUnit.MILLISECONDS)).thenReturn(contentsOfFile);


        when(gitHubService.retrieveFromUrl(file.getRawUrl())).thenReturn(completableFutureRawFile);

        assertThat(frameworkDetectionEngine.detectFramework(files)).isEqualTo("Spring");

    }

    @Test
    void detectsSymfonyFromComposerJson() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        List<File> files = Lists.newArrayList();
        File file = new File();
        file.setFilename("composer.json");
        file.setRawUrl("rawurl.com");
        files.add(file);
        CompletableFuture<String> completableFutureRawFile = mock(CompletableFuture.class);

        String contentsOfFile = "{\n" +
                "    \"name\": \"symfony/symfony-demo\",\n" +
                "    \"license\": \"MIT\",\n" +
                "    \"type\": \"project\",\n" +
                "    \"description\": \"Symfony Demo Application\",\n" +
                "    \"require\": {\n" +
                "        \"php\": \"^7.1.3\",\n" +
                "        \"ext-pdo_sqlite\": \"*\",\n" +
                "        \"doctrine/doctrine-bundle\": \"^1.6.10\",\n" +
                "        \"doctrine/doctrine-migrations-bundle\": \"^1.3\",\n" +
                "        \"doctrine/orm\": \"^2.5.11\",\n" +
                "        \"erusev/parsedown\": \"^1.6\",\n" +
                "        \"ezyang/htmlpurifier\": \"^4.9\",\n" +
                "        \"sensio/framework-extra-bundle\": \"^5.1\",\n" +
                "        \"sensiolabs/security-checker\": \"^5.0\",\n" +
                "        \"symfony/asset\": \"*\",\n" +
                "        \"symfony/console\": \"*\",\n" +
                "        \"symfony/expression-language\": \"*\",\n" +
                "        \"symfony/flex\": \"^1.1\",\n" +
                "        \"symfony/form\": \"*\",\n" +
                "        \"symfony/framework-bundle\": \"*\",\n" +
                "        \"symfony/intl\": \"*\",\n" +
                "        \"symfony/monolog-bundle\": \"^3.1\",\n" +
                "        \"symfony/polyfill-php72\": \"^1.8\",\n" +
                "        \"symfony/security-bundle\": \"*\",\n" +
                "        \"symfony/swiftmailer-bundle\": \"^3.1\",\n" +
                "        \"symfony/translation\": \"*\",\n" +
                "        \"symfony/twig-bundle\": \"*\",\n" +
                "        \"symfony/validator\": \"*\",\n" +
                "        \"symfony/webpack-encore-bundle\": \"^1.1\",\n" +
                "        \"symfony/yaml\": \"*\",\n" +
                "        \"twig/extensions\": \"^1.5\",\n" +
                "        \"twig/twig\": \"^2.6\",\n" +
                "        \"white-october/pagerfanta-bundle\": \"^1.1\"\n" +
                "    },\n" +
                "    \"require-dev\": {\n" +
                "        \"dama/doctrine-test-bundle\": \"^5.0\",\n" +
                "        \"doctrine/doctrine-fixtures-bundle\": \"^3.0\",\n" +
                "        \"friendsofphp/php-cs-fixer\": \"^2.12\",\n" +
                "        \"symfony/browser-kit\": \"*\",";

        when(completableFutureRawFile.get(10000, TimeUnit.MILLISECONDS)).thenReturn(contentsOfFile);


        when(gitHubService.retrieveFromUrl(file.getRawUrl())).thenReturn(completableFutureRawFile);

        assertThat(frameworkDetectionEngine.detectFramework(files)).isEqualTo("Symfony");

    }

    @Test
    void detectsLaravelFromComposerJson() throws IOException, InterruptedException, ExecutionException, TimeoutException {
        List<File> files = Lists.newArrayList();
        File file = new File();
        file.setFilename("composer.json");
        file.setRawUrl("rawurl.com");
        files.add(file);
        CompletableFuture<String> completableFutureRawFile = mock(CompletableFuture.class);

        String contentsOfFile = "{\n" +
                "    \"name\": \"laravel/laravel\",\n" +
                "    \"type\": \"project\",\n" +
                "    \"description\": \"The Laravel Framework.\",\n" +
                "    \"keywords\": [\n" +
                "        \"framework\",\n" +
                "        \"laravel\"\n" +
                "    ],";

        when(completableFutureRawFile.get(10000, TimeUnit.MILLISECONDS)).thenReturn(contentsOfFile);


        when(gitHubService.retrieveFromUrl(file.getRawUrl())).thenReturn(completableFutureRawFile);

        assertThat(frameworkDetectionEngine.detectFramework(files)).isEqualTo("Laravel");

    }


}