package diss.utils;

import diss.parser.utils.ParserUtils;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserUtilsTest {

    private String exampleURI;

    @Test
    public void getsPathFromUri() {
        exampleURI = "https://diss.api.GitHub.com/django/django/";
        assertThat(ParserUtils.getPathFromGitHubUri(exampleURI)).isEqualTo("/django/django/");
    }

}