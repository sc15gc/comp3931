package diss.parser;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AccessTokenParserTest {

    private AccessTokenParser accessTokenParser = new AccessTokenParser();

    @Test
    void parsesAccessToken() {
        String testString = "access_token=646add22bc18267732e5e8b5ac8c88721202a2a4&scope=&token_type=bearer";
        assertThat(accessTokenParser.parseAccessCode(testString)).isEqualTo("646add22bc18267732e5e8b5ac8c88721202a2a4");
    }

}