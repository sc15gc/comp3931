package diss.parser;


import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class UserParserTest {

    private static UserParser userParser;
    private static JSONObject userJson = new JSONObject();


    @BeforeAll
    static void setUp() {
        userParser = new UserParser();

        userJson.put("login", "Django");
        userJson.put("avatar_url", "https://avatars2.githubusercontent.com/u/27804?v=4");
        userJson.put("email", "django@email.com");
    }

    @Test
    void parsesName() {
        assertThat(userParser.parseUser(userJson).getName()).isEqualTo("Django");
    }

    @Test
    void parsesEmail() {
        assertThat(userParser.parseUser(userJson).getEmail()).isEqualTo("django@email.com");
    }

    @Test
    void parsesAvatarUrl() {
        assertThat(userParser.parseUser(userJson).getAvatarUrl()).isEqualTo("https://avatars2.githubusercontent.com/u/27804?v=4");
    }


}