package diss.parser;

import diss.api.domain.File;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RepositoryFilesParserTest {

    private RepositoryFilesParser repositoryFilesParser = new RepositoryFilesParser();

    private JSONObject contentsJson;
    private int amountOfFiles;

    @BeforeEach
    void setUp() {
        contentsJson = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        contentsJson.put("contents", jsonArray);
        JSONObject firstFileJson = new JSONObject();
        firstFileJson.put("name", ".gitignore");

        JSONObject secondFileJson = new JSONObject();
        secondFileJson.put("name", "readme.md");
        secondFileJson.put("download_url", "raw download url");

        JSONObject thirdFileJson = new JSONObject();
        thirdFileJson.put("name", ".idea");

        JSONObject fourthFileJson = new JSONObject();
        fourthFileJson.put("name", "Dockerfile");

        JSONObject fifthFileJson = new JSONObject();
        fifthFileJson.put("name", ".circleci");

        jsonArray.put(firstFileJson);
        jsonArray.put(secondFileJson);
        jsonArray.put(thirdFileJson);
        jsonArray.put(fourthFileJson);
        jsonArray.put(fifthFileJson);

        amountOfFiles = jsonArray.length();

    }

    @Test
    void detectsGitIgnore() {
        assertThat(repositoryFilesParser.hasGitIgnore(contentsJson)).isTrue();
    }

    @Test
    void detectsWhenGitIgnoreIsNotPresent() {
        assertThat(repositoryFilesParser.hasGitIgnore(setUpPlainContents())).isFalse();
    }

    @Test
    void detectsIdeaFolderWhenPresent() {
        assertThat(repositoryFilesParser.hasIdeaFolder(contentsJson)).isTrue();
    }

    @Test
    void detectsWhenIdeaFolderIsNotPresent() {
        assertThat(repositoryFilesParser.hasIdeaFolder(setUpPlainContents())).isFalse();
    }

    @Test
    void retrievesAllFiles() {
        assertThat(repositoryFilesParser.retrieveAllFiles(contentsJson)).hasSize(amountOfFiles);
    }

    @Test
    void detectsWhenDockerfileIsPresent() {
        assertThat(repositoryFilesParser.hasDockerfile(contentsJson)).isTrue();
    }

    @Test
    void detectsWhenCircleCiFileIsPresent() {
        assertThat(repositoryFilesParser.hasCircleCi(contentsJson)).isTrue();
    }

    @Test
    void detectsWhenCircleCiFileIsNotPresent() {
        assertThat(repositoryFilesParser.hasCircleCi(setUpPlainContents())).isFalse();
    }

    @Test
    void extractsReadMeFile() {
        File readme = repositoryFilesParser.extractReadMeFile(contentsJson);

        assertThat(readme.getRawUrl()).isEqualTo("raw download url");
        assertThat(readme.getFilename()).isEqualTo("readme.md");
    }


    private JSONObject setUpPlainContents() {
        JSONObject contentsJson = new JSONObject();
        JSONArray contentsArray = new JSONArray();
        contentsJson.put("contents", contentsArray);
        JSONObject firstFile = new JSONObject();
        firstFile.put("name", "firstFile");

        contentsArray.put(firstFile);
        return contentsJson;
    }
}