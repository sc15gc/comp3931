package diss.parser;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ContributionsParserTest {

    private static JSONObject contributor;
    private static ContributionsParser contributionsParser;

    @BeforeAll
    static void setUp() {
        contributor = new JSONObject();
        contributor.put("contributions", 1000);
        contributionsParser = new ContributionsParser();
    }


    @Test
    void parsesContributors() {
        assertThat(contributionsParser.parseContributions(contributor)).isEqualTo(1000);
    }


}