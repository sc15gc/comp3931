package diss.builder;

import com.google.common.collect.Lists;
import diss.ApplicationConfiguration;
import diss.api.builder.CommitListBuilder;
import diss.api.domain.Commit;
import diss.api.domain.File;
import diss.api.domain.analysis.CommitBulkAnalyser;
import diss.api.domain.analysis.CommitListAnalysis;
import diss.domain.Analysis;
import diss.lint_utils.PmdAnalysisUtil;
import diss.lint_utils.domain.Suggestion;
import diss.parser.AheadByParser;
import diss.parser.RepositoryDetailsParser;
import diss.parser.RepositoryFilesParser;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest({ApplicationConfiguration.class})
class AnalysisBuilderTest {

    //TODO: implement power mock to mock the retrieval of git repositories


    @MockBean
    private CommitListBuilder commitListBuilder;

    @MockBean
    private RepositoryDetailsParser repositoryDetailsParser;

    @MockBean
    private RepositoryFilesParser repositoryFilesParser;

    @MockBean
    private CommitBulkAnalyser commitBulkAnalyser;

    @MockBean
    private AheadByParser aheadByParser;

    @MockBean
    private PmdAnalysisUtil pmdAnalysisUtil;

    @Autowired
    private AnalysisBuilder analysisBuilder;

    private Analysis analysis;

    @BeforeEach
    void setUp() {
        JSONObject jsonReturn = new JSONObject();
        JSONObject contents = new JSONObject();
        JSONObject commits = new JSONObject();
        JSONObject comparison = new JSONObject();
        analysis = new Analysis();
        when(repositoryDetailsParser.parseLanguage(jsonReturn)).thenReturn("python");
        when(repositoryDetailsParser.parseRepoName(jsonReturn)).thenReturn("repoName");
        when(repositoryDetailsParser.parsePrivacySetting(jsonReturn)).thenReturn(true);
        when(repositoryFilesParser.hasGitIgnore(contents)).thenReturn(true);
        when(repositoryFilesParser.hasIdeaFolder(contents)).thenReturn(true);
        when(repositoryFilesParser.hasCircleCi(contents)).thenReturn(true);

        File readme = new File();
        readme.setRawUrl("download url");
        readme.setFilename("readme.md");
        when(repositoryFilesParser.extractReadMeFile(contents)).thenReturn(readme);
        String repoURI = "exampleURI";
        String accessToken = "accessToken";

        Commit firstCommit = new Commit();
        firstCommit.setMessage("Message 1");

        CommitListAnalysis commitAnalysis = new CommitListAnalysis();
        commitAnalysis.setAverageLengthOfCommitMessage(17);

        List<Commit> commitList = Lists.newArrayList();
        commitList.add(firstCommit);

        when(commitListBuilder.buildCommitList(repoURI, accessToken, commits)).thenReturn(commitList);
        when(commitBulkAnalyser.analyse(commitList)).thenReturn(commitAnalysis);
        when(aheadByParser.retrieveAheadBy(comparison)).thenReturn(6);

        ArrayList<Suggestion> suggestionsList = Lists.newArrayList();
        suggestionsList.add(new Suggestion("firstFilePath", "suggestion 1", repoURI, "1"));
        suggestionsList.add(new Suggestion("secondFilePath", "suggestion 2", repoURI, "1"));

        when(pmdAnalysisUtil.generateSuggestionsForCodeInRepository(repoURI, accessToken)).thenReturn(suggestionsList);

        analysis = analysisBuilder.buildAnalysis(repoURI, accessToken, jsonReturn, contents, commits, comparison, true);
    }

    @Test
    void hasLanguage() {
        assertThat(analysis.getLanguage()).isEqualTo("python");
    }

    @Test
    void hasRepoName() {
        assertThat(analysis.getRepoName()).isEqualTo("repoName");
    }

    @Test
    void hasAmountOfCommits() {
        assertThat(analysis.getAmountOfCommits()).isEqualTo(7);
    }

    @Test
    void hasCircleCi() {
        assertThat(analysis.isCircleCiPresent()).isEqualTo(true);
    }

    @Test
    void hasGitIgnore() {
        assertThat(analysis.isGitIgnorePresent()).isEqualTo(true);
    }

    @Test
    void hasReadMe() {
        assertThat(analysis.isReadMePresent()).isEqualTo(true);
    }

    @Test
    void detectsWhenReadMeIsNotPresent() {
        File readMeFile = new File();
        readMeFile.setRawUrl(null);
        analysis.setReadMeFile(readMeFile);
        assertThat(analysis.isReadMePresent()).isEqualTo(false);
    }

    @Test
    void hasIdeaFolder() {
        assertThat(analysis.isIdeaFolderPresent()).isEqualTo(true);
    }

    @Test
    void commitListPresent() {
        assertThat(analysis.getCommits()).hasSize(1);
        assertThat(analysis.getCommits().get(0).getMessage()).isEqualTo("Message 1");
    }

    @Test
    void commitListAnalysisPresent() {
        assertThat(analysis.getCommitListAnalysis().getAverageLengthOfCommitMessage()).isEqualTo(17);
    }

    @Test
    void hasReadMeFileLink() {
        assertThat(analysis.getReadMeFile().getRawUrl()).isEqualTo("download url");
        assertThat(analysis.getReadMeFile().getFilename()).isEqualTo("readme.md");
    }

    @Test
    void hasPrivacySetting() {
        assertThat(analysis.isPrivate()).isEqualTo(true);
    }

    @Test
    void hasLintOutput() {
        assertThat(analysis.getLintOutput()).hasSize(2);
        assertThat(analysis.getLintOutput().get(0).getSuggestionText()).isEqualTo("suggestion 1");
    }


}